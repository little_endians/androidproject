package com.example.apple.bambiniapp.entityServices;

import com.example.apple.bambiniapp.dataModelsToCrud.PostCommentDatas;
import com.example.apple.bambiniapp.entities.Student;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface SendStudentsNotInClass {

    @POST("myoklama/{classid}")
    Call<ResponseBody> postStudents(@Path("classid") int classid,@Body ArrayList<Student> liste);
}
