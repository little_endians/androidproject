package com.example.apple.bambiniapp.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Question implements Parcelable {

    @SerializedName("q_id")
    @Expose
    private int q_id;

    @SerializedName("u_id")
    @Expose
    private int user_id;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("t_id")
    @Expose
    private int teacher_id;

    @SerializedName("title")
    @Expose
    private String question_title;

    @SerializedName("text")
    @Expose
    private String question_text;

    @SerializedName("date")
    @Expose
    private String question_date;


    @SerializedName("subject")
    @Expose
    private String subject;

    public Question(int q_id,int user_id, String username, int teacher_id, String text,String date,String subject,String q_tag) {
        this.q_id = q_id;
        this.user_id = user_id;
        this.username = username;
        this.teacher_id = teacher_id;
        this.question_title = q_tag;
        this.question_text = text;
        this.question_date = date;
        this.subject = subject;
    }
    public Question() {

    }

    public String getQuestion_text() {
        return question_text;
    }

    public String getQuestion_title() {
        return question_title;
    }

    public String getQuestion_date() {
        return question_date;
    }

    public int getTeacher_id() {
        return teacher_id;
    }

    public int getQ_id() {
        return q_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public Question(Parcel in){
        String[] data= new String[3];

        in.readStringArray(data);


        this.question_title= (data[0]);
        this.question_text= (data[1]);
        this.question_date= (data[2]);
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeStringArray(new String[]{this.question_title,this.question_text,this.question_date});
    }

    public static final Parcelable.Creator<Question> CREATOR= new Parcelable.Creator<Question>() {

        @Override
        public Question createFromParcel(Parcel source) {

            return new Question(source);  //using parcelable constructor
        }

        @Override
        public Question[] newArray(int size) {

            return new Question[size];
        }
    };

}
