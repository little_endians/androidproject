package com.example.apple.bambiniapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.apple.bambiniapp.adapters.OurAdapter;
import com.example.apple.bambiniapp.adapters.adapterForAnnouncementPage;
import com.example.apple.bambiniapp.entities.Announcement;
import com.example.apple.bambiniapp.entities.Comment;
import com.example.apple.bambiniapp.entityServices.AnnouncementWhoSawService;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.example.apple.bambiniapp.mainpageTeachersActivity.announcementsList;

public class announcementActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_EDIT = 302;
    public static final int REQUEST_CODE_COMMENTT = 1908;
    static WhoSawAnnLoader WL;
    static WhoPollWhatLoader WP;
    ListView announcementInfoList;
    ArrayList<Object> toplam;
    ArrayList<Announcement> announcement = new ArrayList<Announcement>();
    ArrayList<String> whosawList;
    ArrayList<String> whopollListYes,whopollListNo;
    ArrayList<Comment> comments;
    adapterForAnnouncementPage adapterX;
    static CommentLoader CL ;
    String className = "";
    int position;
    int ntf_id;
    private static boolean noteEdited = false;
    Comment c;
    int commentTouch = 0;

    String username;
    int user_id;

    String editedTitle = " ",editedContent = " ",editedDate = " ",editedornot= " ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_announcement);

        className = getIntent().getStringExtra("className");
        position = getIntent().getIntExtra("position",0);
        username = getIntent().getStringExtra("username");
        user_id = getIntent().getIntExtra("user_id",0);

        announcement.add(announcementsList.get(position));

        Announcement answerTAG = new Announcement(0, "", "Cevapları görmek için buraya tıklayın.", "");
        announcement.add(answerTAG);
        Announcement commentTAG = new Announcement(0, "", "                      YORUMLAR", "");
        announcement.add(commentTAG);

        ntf_id = announcementsList.get(position).getntf_id();

        WL = new WhoSawAnnLoader(ntf_id);
        WL.execute();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        whosawList = WL.whosawList;

        comments =new ArrayList<>();

        announcementInfoList = (ListView) findViewById(R.id.oneAnnouncementListview);
        adapterX = new adapterForAnnouncementPage(this,R.layout.layout_textviewfor1announcement,announcement);
        announcementInfoList.setAdapter(adapterX);

        announcementInfoList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View v, int position, long id){

                if(position == 2){

                    if(commentTouch == 0){
                    CL = new CommentLoader(ntf_id); // must be initialized here to do cont. tasks
                    CL.execute();

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    comments = CL.commentList;
                    ArrayList<Announcement> commentS = new ArrayList<Announcement>();

                    for (int i = 0; i<comments.size(); i++){
                        commentS.add(i,new Announcement(comments.get(i).getComment_id(),comments.get(i).getWriter_name() + "'dan yorum :",comments.get(i).getComment(), " "));
                    }


                    announcement.addAll(commentS);
                    adapterX.notifyDataSetChanged();
                    commentTouch = commentTouch + 1;}
                }

                if(position == 1){

                    WP = new WhoPollWhatLoader(ntf_id);
                    WP.execute();

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    whopollListYes = WP.whoPollListYes;
                    whopollListNo = WP.whoPollListNo;

                    StringBuilder listStringEvet= new StringBuilder();
                    StringBuilder listStringHayir= new StringBuilder();

                    if(whopollListYes!=null) {
                        for (String list : whopollListYes){
                            listStringEvet.append(list + "\n");
                            System.out.println(list);}
                    }else{
                        listStringEvet.append(" ");
                    }

                    if(whopollListNo != null) {
                        for (String list : whopollListNo)
                            listStringHayir.append(list + "\n");
                    }else{
                        listStringHayir.append(" ");
                    }

                    AlertDialog.Builder builder = new AlertDialog.Builder(announcementActivity.this);
                    builder.setMessage("EVET CEVAPLARI:\n" +listStringEvet + "\n" + "HAYIR CEVAPLARI:\n" + listStringHayir)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }

            }
        });

        announcementInfoList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int arg2, long arg3) {

                if(arg2 == 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(announcementActivity.this);
                    builder.setTitle("Yorum yapmak istiyor musunuz ?");
                    builder.setPositiveButton("EVET", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            commentingTeacher(position);
                            dialog.dismiss();
                        }
                    });
                    builder.setNegativeButton("HAYIR", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }

                return true;
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("TAGstart", "On Start .....");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("TAGstop", "On Stop .....");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("TAGdestroy", "On Destroy .....");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("TAGpause", "On Pause .....");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("TAGrestart", "On Restart .....");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("TAGresume", "On Resume .....");

        if(editedornot.equals("true") && !editedContent.equals(" ") && !editedTitle.equals(" ")){
            Log.e("WWWQQQQ","SSLSLSLSLSLSLSLLSLSL");
            Announcement ann = new Announcement(ntf_id,editedTitle,editedContent,editedDate);
            announcementsList.set(position,ann);
            announcement.remove(0);
            announcement.add(0,ann);

            announcementInfoList = (ListView) findViewById(R.id.oneAnnouncementListview);
            adapterForAnnouncementPage adapterX = new adapterForAnnouncementPage(this,R.layout.layout_textviewfor1announcement,announcement);
            announcementInfoList.setAdapter(adapterX);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuItem classNameItem;
        getMenuInflater().inflate(R.menu.actionbar_menuitems_announcement, menu);
        classNameItem = menu.findItem(R.id.action_class_name);
        classNameItem.setTitle(className);

        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (REQUEST_CODE_EDIT) : {

                if (resultCode == Activity.RESULT_OK) {

                    editedTitle = data.getStringExtra("editedTitle");
                    editedContent = data.getStringExtra("editedContent");
                    editedDate = data.getStringExtra("editedDate");
                    editedornot = data.getStringExtra("edited");
                    Log.e("WWWQQQQ",editedContent + editedornot + editedTitle);

                }
                break;
            }
            case (REQUEST_CODE_COMMENTT) : {

                if (resultCode == Activity.RESULT_OK) {
                    c = data.getParcelableExtra("addedComment");
                    Announcement an = new Announcement(c.getComment_id(),c.getComment_tag(),c.getComment(), " ");
                    announcement.add(an);
                    adapterX.notifyDataSetChanged();
                }
                break;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_edit:
                noteEdited = true;
                Toast.makeText(this, "Edit note selected", Toast.LENGTH_SHORT)
                        .show();
                Intent intent = new Intent(this, editAnnouncementActivity.class);
                intent.putExtra("title",announcementsList.get(position).getAnnouncement_tag());
                intent.putExtra("content",announcementsList.get(position).getAnnouncement_content());
                intent.putExtra("date",announcementsList.get(position).getAnnouncement_date());
                intent.putExtra("ntf_id",announcementsList.get(position).getntf_id());
                startActivityForResult(intent,REQUEST_CODE_EDIT);
                break;
            case android.R.id.home:

                onBackPressed();
                return true;
            case R.id.who_saw:
                StringBuilder listString = new StringBuilder();

                for (String liste : whosawList)
                    listString.append(liste+"\n");

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(listString)
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //do things
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
                break;
            default:
                break;
        }
        return true;
    }

    public void commentingTeacher(int position){

        Intent intent = new Intent(this,CommentingTeacherActivity.class);
        intent.putExtra("user_id",user_id);
        intent.putExtra("username",username);
        intent.putExtra("ntf_id",announcementsList.get(position).getntf_id());
        startActivityForResult(intent,REQUEST_CODE_COMMENTT);
    }

}