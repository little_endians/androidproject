package com.example.apple.bambiniapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.apple.bambiniapp.entities.Announcement;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class addAnnouncementActivity extends AppCompatActivity {

    public static final int REQUEST_CODE = 202;

    EditText titleText,priorityText,announcementText;
    Button sendAnnButton;

    String title,priority,content,creationdate,classname;
    Announcement announcement;
    int newAnnouncementid,user_id;
    String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_announcement);

        titleText = (EditText) findViewById(R.id.titleText);
        priorityText = (EditText) findViewById(R.id.priorityText);
        announcementText = (EditText) findViewById(R.id.annText);
        sendAnnButton = (Button) findViewById(R.id.commentButton);
        newAnnouncementid = getIntent().getIntExtra("newAnnouncementid",0);

        announcement = new Announcement(newAnnouncementid,title,content,creationdate);

        sendAnnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                title = titleText.getText().toString();
                priority = priorityText.getText().toString();
                content = announcementText.getText().toString();

                creationdate = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime());
                user_id = getIntent().getIntExtra("user_id",23);
                username = getIntent().getStringExtra("username");

                String classname = getIntent().getStringExtra("classname");
                NewAnnouncementPost annPost = new NewAnnouncementPost();
                annPost.postingAnn(user_id,username,title,content,creationdate,"tartisma");
                Toast.makeText(getApplicationContext(), "Duyuru eklendi ...",Toast.LENGTH_SHORT).show();
                sendAnnGoBack(view);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        getMenuInflater().inflate(R.menu.addannouncement_menuitems, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void sendAnnGoBack(View view) {

        Intent intent = new Intent(this, mainpageTeachersActivity.class);
        Announcement announcement = new Announcement(newAnnouncementid,title,content,creationdate);
        intent.putExtra("addedAnnouncement",announcement);
        setResult(Activity.RESULT_OK,intent);
        finish();

    }

}