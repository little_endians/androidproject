package com.example.apple.bambiniapp.entities;

public class Parent extends User {

    public Parent(int u_id, String name, String surname, String phone, String email, String pwd) {
        super(u_id, name, surname, phone, email, pwd);
    }
}
