package com.example.apple.bambiniapp;

import android.util.Log;

import com.example.apple.bambiniapp.entityServices.DeleteAnnouncementService;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DeleteAnnouncement {

    RetrofitConnection rc = new RetrofitConnection();
    Retrofit retrofit = rc.getRetrofit();

    public void deleteAnn(int ntf_id) {

        DeleteAnnouncementService deleteService = retrofit.create(DeleteAnnouncementService.class);
        Call<ResponseBody> call = deleteService.delete(ntf_id);

        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {

                    try {
                        Log.e("Ann is deleted", response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("FAIL", "onFailure ");

            }
        });

    }
}