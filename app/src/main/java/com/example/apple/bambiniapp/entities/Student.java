package com.example.apple.bambiniapp.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Student implements Parcelable {

    @SerializedName("s_id")
    @Expose
    private int s_id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("surname")
    @Expose
    private String surname;

    @SerializedName("courselist")
    @Expose
    private String courselist;

    @SerializedName("class_section")
    @Expose
    private String class_section;

    @SerializedName("onay")
    @Expose
    private String onay;

    @SerializedName("p_id")
    @Expose
    private String p_id;

    public Student(int s_id, String name, String surname, String courselist, String class_section, String onay, String p_id) {
        this.s_id = s_id;
        this.name = name;
        this.surname = surname;
        this.courselist = courselist;
        this.class_section = class_section;
        this.onay = onay;
        this.p_id = p_id;
    }

    public int getStudent_id() {
        return s_id;
    }

    public void setStudent_id(int s_id) {
        this.s_id = s_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCourselist() {
        return courselist;
    }

    public void setCourselist(String courselist) {
        this.courselist = courselist;
    }

    public String getClass_section() {
        return class_section;
    }

    public void setClass_section(String class_section) {
        this.class_section = class_section;
    }

    public String getOnay() {
        return onay;
    }

    public void setOnay(String onay) {
        this.onay = onay;
    }

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public Student(Parcel in){
        String[] data= new String[7];

        in.readStringArray(data);

        this.s_id = Integer.parseInt(data[0]);
        this.name = data[1];
        this.surname = data[2];
        this.courselist = data[3];
        this.class_section = data[4];
        this.onay = data[5];
        this.p_id = data[6];

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeStringArray(new String[]{String.valueOf(this.s_id),this.name,this.surname,this.courselist,this.class_section,this.onay,this.p_id});
    }

    public static final Parcelable.Creator<Student> CREATOR= new Parcelable.Creator<Student>() {

        @Override
        public Student createFromParcel(Parcel source) {

            return new Student(source);  //using parcelable constructor
        }

        @Override
        public Student[] newArray(int size) {

            return new Student[size];
        }
    };

}
