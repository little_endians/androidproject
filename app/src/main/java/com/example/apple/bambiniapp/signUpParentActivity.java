package com.example.apple.bambiniapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.apple.bambiniapp.entities.Parent;
import com.example.apple.bambiniapp.entities.User;
import com.example.apple.bambiniapp.entityServices.ParentService;


import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class signUpParentActivity extends AppCompatActivity {

    RetrofitConnection rc = new RetrofitConnection();
    Retrofit retrofit = rc.getRetrofit();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_parent);

        Button signButton = (Button) findViewById(R.id.kayitOlText);
        final TextView nameBox = (TextView) findViewById(R.id.isimText);
        final TextView surnameBox = (TextView) findViewById(R.id.soyisimText);
        final TextView emailBox = (TextView) findViewById(R.id.mailText);
        final TextView phoneBox = (TextView) findViewById(R.id.cepTelefonuText);
        final TextView passwordBox = (TextView) findViewById(R.id.sifreText);

        signButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                User user = new Parent(1,nameBox.getText().toString(),surnameBox.getText().toString(),
                        phoneBox.getText().toString(), emailBox.getText().toString(),passwordBox.getText().toString());


                ParentService service = retrofit.create(ParentService.class);
                Call<ResponseBody> createCall = service.create(user);

                createCall.enqueue(new Callback<ResponseBody>() {

                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        if (response.isSuccessful()) {
                            try {

                                String stringResponse = response.body().string();
                                Log.e("cevapVeli",stringResponse);
                                mainActivity(view);

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("FAIL","onFailure ");

                    }
                });

            }
        });

    }
    public void mainActivity(View view) {

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }
}
