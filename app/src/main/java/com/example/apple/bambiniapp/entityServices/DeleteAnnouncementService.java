package com.example.apple.bambiniapp.entityServices;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface DeleteAnnouncementService {

    @POST("mduyuruSil/{ntf_id}")
    Call<ResponseBody> delete(@Path("ntf_id") int ntf_id);
}
