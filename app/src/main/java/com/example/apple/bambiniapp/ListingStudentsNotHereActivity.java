package com.example.apple.bambiniapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.example.apple.bambiniapp.adapters.adapterForStudentPage;
import com.example.apple.bambiniapp.entities.Student;

import java.util.ArrayList;

public class ListingStudentsNotHereActivity extends AppCompatActivity {

    ListView ourList;
    ArrayList<Student> studentsNotHere = new ArrayList<>();
    String classhour;
    int hourOfclass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_students_not_here);

        studentsNotHere = getIntent().getParcelableArrayListExtra("studentsnothere");
        classhour = getIntent().getStringExtra("classhour");
        hourOfclass = Integer.parseInt(classhour.substring(0,1));


        ourList = (ListView) findViewById(R.id.nothereListview);
        adapterForStudentPage adapterX = new adapterForStudentPage(this,R.layout.layout_textviewforlistr,studentsNotHere);
        ourList.setAdapter(adapterX);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.actionbar_menuitems_nothere, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_geri:
                turnBackHereorNot();
                return true;
            case R.id.action_onay:
                NewHereOrNotListPost sendListPost = new NewHereOrNotListPost();
                sendListPost.postingHereOrNotList(hourOfclass,studentsNotHere);

                HereorNotAcceptencePost xxx = new HereorNotAcceptencePost();
                xxx.sendAcceptence();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void turnBackHereorNot(){
        Intent intent = new Intent(this, StudentsInClassOrNotActivity.class);
        setResult(Activity.RESULT_OK,intent);
        finish();

    }


}
