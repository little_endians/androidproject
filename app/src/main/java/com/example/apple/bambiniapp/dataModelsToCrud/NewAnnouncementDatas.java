package com.example.apple.bambiniapp.dataModelsToCrud;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewAnnouncementDatas {

    @SerializedName("ntf_id")
    @Expose
    private int ntf_id;

    @SerializedName("user_id")
    @Expose
    private int user_id;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("text")
    @Expose
    private String content;

    @SerializedName("date")
    @Expose
    private String creationdate;

    @SerializedName("tarih")
    @Expose
    private String anntarih;

    @SerializedName("classname")
    @Expose
    private String classname;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("baslangic")
    @Expose
    private String baslangic;

    @SerializedName("bitis")
    @Expose
    private String bitis;

    @SerializedName("konum")
    @Expose
    private String konum;

    @SerializedName("tur")
    @Expose
    private String tur;

    public NewAnnouncementDatas(int user_id, String writerName, String title, String content, String creationdate, String type) {
        this.user_id = user_id;
        this.title = title;
        this.content = content;
        this.creationdate = creationdate;
        this.username = writerName;
        this.type = type;
    }

    //mozelDuyuru
    public NewAnnouncementDatas(String anntarih, String baslangic, String bitis, String konum, int user_id, String username, String tur,String creationdate) {
        this.anntarih = anntarih;
        this.baslangic = baslangic;
        this.bitis = bitis;
        this.konum = konum;
        this.user_id = user_id;
        this.username = username;
        this.tur = tur;
        this.creationdate = creationdate;
    }
}