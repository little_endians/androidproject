package com.example.apple.bambiniapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class askQuestionActivity extends AppCompatActivity {

    EditText askQTitleText, context;
    Spinner spinner;
    Button askQuestionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_question);

        askQTitleText = (EditText) findViewById(R.id.askQTitle);
        context = (EditText) findViewById(R.id.askQcontext);
        spinner = (Spinner) findViewById(R.id.spinner);
        askQuestionButton = (Button) findViewById(R.id.askQuestionButton);

        askQuestionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int teacher_id = 0;
                String teacher = spinner.getSelectedItem().toString();
                if (teacher.equals("Arda Göktürk Eyüboğlu"))
                    teacher_id = 23;
                if (teacher.equals("Ozan Selçuk Gümüş"))
                    teacher_id = 2;
                String title = askQTitleText.getText().toString();
                String content = context.getText().toString();
                String date = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime());
                int user_id = getIntent().getIntExtra("user_id", 0);
                String username = getIntent().getStringExtra("username");
                Log.e("askquestion", teacher + " " + content + " " + username + " " + String.valueOf(user_id));

                NewQuestionPost askPost = new NewQuestionPost();
                askPost.postingQuestion(user_id, username, teacher_id, title, content, date);

                finish();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void goBackAnnouncement(View view) {


        Intent intent = new Intent(this, mainpageParentsActivity.class);
        startActivity(intent);
    }

}