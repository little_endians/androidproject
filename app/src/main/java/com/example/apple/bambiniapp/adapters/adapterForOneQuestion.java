package com.example.apple.bambiniapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.apple.bambiniapp.R;
import com.example.apple.bambiniapp.entities.Question;

import java.util.ArrayList;

public class adapterForOneQuestion extends ArrayAdapter<Question> {
    ArrayList<Question> queList = new ArrayList<>();

    public adapterForOneQuestion(Context context, int textViewResourceId, ArrayList<Question> objects) {
        super(context, textViewResourceId, objects);
        queList = objects;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.layout_textviewfor1question, null);

        TextView textView = (TextView) v.findViewById(R.id.qtagTextView);
        TextView textView2 = (TextView) v.findViewById(R.id.questionTextView);
        TextView textView3 = (TextView) v.findViewById(R.id.questionDateView);

        // textView.setText(queList.get(position).getQuestion_title());
        textView2.setText(queList.get(position).getQuestion_text());
        textView3.setText(queList.get(position).getQuestion_date());

        return v;

    }
}
