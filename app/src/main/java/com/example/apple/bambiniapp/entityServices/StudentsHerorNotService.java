package com.example.apple.bambiniapp.entityServices;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface StudentsHerorNotService {

    @GET("msinifSec/{sinif}")
    Call<ResponseBody> getClassStudents(@Path("sinif") String classname);
}
