package com.example.apple.bambiniapp.dataModelsToCrud;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StudentDatas {

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("surname")
    @Expose
    private String surname;

    @SerializedName("classname")
    @Expose
    private String classname;

    @SerializedName("user_id")
    @Expose
    private int user_id;

    public StudentDatas(String name, String surname, String classname, int user_id) {
        this.name = name;
        this.surname = surname;
        this.classname = classname;
        this.user_id = user_id;
    }
}
