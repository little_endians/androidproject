package com.example.apple.bambiniapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.apple.bambiniapp.adapters.OurAdapter;
import com.example.apple.bambiniapp.adapters.adapterForSlideMenu;
import com.example.apple.bambiniapp.entities.Announcement;
import com.example.apple.bambiniapp.entities.User;

import java.util.ArrayList;

public class mainpageTeachersActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_ADD = 201;
    public static final int REQUEST_CODE_ADD_MEETING = 532;
    public static final int REQUEST_CODE_ADD_TRAVEL = 541;
    public static final int REQUEST_CODE_ADD_YESNO = 554;
    public static final int REQUEST_CODE_ANNOUNCEMENT = 1538;
    public static final int REQUEST_CODE_STUDENTREQUESTS = 69;
    public static final int REQUEST_CODE_STUDENT_HEREORNOT = 1299;
    public static final int InitActivityToStudentHereorNot = 999;
    public static final int REQUEST_CODE_List = 699;

    ListView ourList;
    ListView panelList;
    static AnnouncementLoader AL ;
    static ArrayList<Announcement> announcementsList = new ArrayList<>();
    static ArrayList<String> panelItems = new ArrayList<>();
    User u;
    Announcement a,sameornot ;

    OurAdapter adapterX;
    int newAnnouncementid=0;

    EditText whosaw;
    String className = "3-A";
    DrawerLayout drawerLayout;

    private static boolean firstTime  = false;

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        Log.e("YYYYYYY","PPPPPPPP");
        savedInstanceState.putParcelable("user", u);savedInstanceState.putParcelableArrayList("announcementsList",announcementsList);
        super.onSaveInstanceState(savedInstanceState);
    }


    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.e("PPPPPP","YYYYYY");
        super.onRestoreInstanceState(savedInstanceState);
        u = savedInstanceState.getParcelable("user");
        announcementsList = savedInstanceState.getParcelableArrayList("announcementsList");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainpage_teachers);

        setPanelItems();
        sameornot = new Announcement(0," "," "," ");

        if(savedInstanceState == null) {
            u = getIntent().getParcelableExtra("user");
            Log.e("Announcement","Execution");

            AL = new AnnouncementLoader(u.getUser_id());
            AL.execute();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            announcementsList = AL.announcementList;
            newAnnouncementid = 0;
            setNewAnnouncementid(newAnnouncementid);
        }

        ourList = (ListView) findViewById(R.id.announcementsListview);
        adapterX = new OurAdapter(this,R.layout.layout_textviewforlistr,announcementsList);
        ourList.setAdapter(adapterX);

        ourList.setOnItemClickListener(new AdapterView.OnItemClickListener(){

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
            goToAnnouncement(view,position);

        }
        });

        panelList = (ListView) findViewById(R.id.panelItemsListview);
        final adapterForSlideMenu adapter = new adapterForSlideMenu(panelItems, getApplicationContext());
        panelList.setAdapter(adapter);

        panelList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                test(view,position);
                //drawerLayout.closeDrawer(panelList);
            }
        });

        ourList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int arg2, long arg3) {
                final int deletePosition =arg2;
                String message = "Silmek istiyor musunuz ?";
                AlertDialog.Builder builder = new AlertDialog.Builder(mainpageTeachersActivity.this);
                builder.setMessage(message)
                        .setCancelable(true)
                        .setPositiveButton("EVET", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                DeleteAnnouncement delete = new DeleteAnnouncement();
                                delete.deleteAnn(announcementsList.get(deletePosition).getntf_id());
                                announcementsList.remove(deletePosition);
                                adapterX = new OurAdapter(mainpageTeachersActivity.this,R.layout.layout_textviewforlistr,announcementsList);
                                ourList.setAdapter(adapterX);
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

                return true;
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (REQUEST_CODE_ADD ) : {

                if (resultCode == Activity.RESULT_OK) {
                    a = data.getParcelableExtra("addedAnnouncement");
                    sameornot.setAnnouncement_content(a.getAnnouncement_content());
                    sameornot.setAnnouncement_tag(a.getAnnouncement_tag());
                    sameornot.setAnnouncement_date(a.getAnnouncement_date());
                }
                break;
            }
            case (REQUEST_CODE_ADD_MEETING) : {

                if (resultCode == Activity.RESULT_OK) {
                    a = data.getParcelableExtra("addedAnnouncement");
                    sameornot.setAnnouncement_content(a.getAnnouncement_content());
                    sameornot.setAnnouncement_tag(a.getAnnouncement_tag());
                    sameornot.setAnnouncement_date(a.getAnnouncement_date());
                }
                break;
            }
            case (REQUEST_CODE_ADD_TRAVEL) : {

                if (resultCode == Activity.RESULT_OK) {
                    a = data.getParcelableExtra("addedAnnouncement");
                    sameornot.setAnnouncement_content(a.getAnnouncement_content());
                    sameornot.setAnnouncement_tag(a.getAnnouncement_tag());
                    sameornot.setAnnouncement_date(a.getAnnouncement_date());
                }
                break;
            }

            case (REQUEST_CODE_ADD_YESNO) : {

                if (resultCode == Activity.RESULT_OK) {
                    a = data.getParcelableExtra("addedAnnouncement");
                    sameornot.setAnnouncement_content(a.getAnnouncement_content());
                    sameornot.setAnnouncement_tag(a.getAnnouncement_tag());
                    sameornot.setAnnouncement_date(a.getAnnouncement_date());
                }
                break;
            }

        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (intent != null)
            setIntent(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("TAGstart", "On Start .....");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("TAGstop", "On Stop .....");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("TAGdestroy", "On Destroy .....");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("TAGpause", "On Pause .....");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("TAGrestart", "On Restart .....");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("TAGresume", "On Resume .....");

        if(a != null && a.getAnnouncement_content().equals(sameornot.getAnnouncement_content()) &&
                a.getAnnouncement_tag().equals(sameornot.getAnnouncement_tag())) {
            announcementsList.add(0, a);
            sameornot.setAnnouncement_content(" ");
        }

        ourList = (ListView) findViewById(R.id.announcementsListview);
        OurAdapter adapterX = new OurAdapter(this,R.layout.layout_textviewforlistr,announcementsList);
        ourList.setAdapter(adapterX);
        AL.cancel(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        MenuItem classNameItem;
        getMenuInflater().inflate(R.menu.actionbar_menuitems, menu);

        classNameItem = menu.findItem(R.id.action_class_name);
        classNameItem.setTitle(className);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_add_note:
                Toast.makeText(this, "Add note selected", Toast.LENGTH_SHORT)
                        .show();

                break;
            case R.id.action_class_name:
                Toast.makeText(this, "Class-name is clicked", Toast.LENGTH_SHORT)
                        .show();
                break;
            default:
                break;
        }

        return true;
    }

    public void goToAnnouncement(View view,int position) {

        Intent intent = new Intent(this, announcementActivity.class);
        intent.putExtra("class_name", className);
        intent.putExtra("position",position);
        intent.putExtra("username",u.getName() + " " + u.getSurname());
        intent.putExtra("user_id",u.getUser_id());
        startActivity(intent);

    }

    public void setPanelItems() {

        panelItems.add("Öğrenci Onay");
        panelItems.add("Yoklama");
        panelItems.add("Soruları Listele");

    }

    public void test(View view, int position)  {

        if(position == 0){

            Intent intent = new Intent(this,StudentRequestsActivity.class);
            startActivityForResult(intent,REQUEST_CODE_STUDENTREQUESTS);
        }
        if(position == 1){

            Intent intent = new Intent(this,InitActivityToStudentHereorNot.class);
            startActivityForResult(intent,InitActivityToStudentHereorNot);
        }
        if(position == 2 ){

            Intent intent = new Intent(this,QuestionActivityTeacher.class);
            intent.putExtra("user_id",u.getUser_id());
            intent.putExtra("classname",className);
            intent.putExtra("name",u.getName() + " " + u.getSurname());
            startActivityForResult(intent,REQUEST_CODE_List);

        }

    }

    public int getNewAnnouncementid() {
        return newAnnouncementid;
    }

    public void setNewAnnouncementid(int newAnnouncementid) {
        this.newAnnouncementid = newAnnouncementid;
    }

    public void discussionAnnounce(MenuItem item){

        Toast.makeText(this, "Tartışma duyurusu girebilirsiniz.", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this,addAnnouncementActivity.class);
        intent.putExtra("user_id",u.getUser_id());
        intent.putExtra("classname",className);
        intent.putExtra("newAnnouncementid",newAnnouncementid);
        intent.putExtra("username",u.getName() + " " + u.getSurname());
        startActivityForResult(intent,REQUEST_CODE_ADD);
    }

    public void yesnoAnnounce(MenuItem item){

        Toast.makeText(this, "Evet/Hayır duyurusu girebilirsiniz.", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this,addYesnoActivity.class);
        intent.putExtra("user_id",u.getUser_id());
        intent.putExtra("classname",className);
        intent.putExtra("newAnnouncementid",newAnnouncementid);
        intent.putExtra("username",u.getName() + " " + u.getSurname());
        startActivityForResult(intent,REQUEST_CODE_ADD_YESNO);
    }

    public void meetingAnnounce(MenuItem item){

        Intent intent = new Intent(this,addMeetingActivity.class);
        intent.putExtra("user_id",u.getUser_id());
        intent.putExtra("classname",className);
        intent.putExtra("newAnnouncementid",newAnnouncementid);
        intent.putExtra("username",u.getName() + " " + u.getSurname());
        startActivityForResult(intent,REQUEST_CODE_ADD_MEETING);
    }

    public void travelAnnounce(MenuItem item){

        Intent intent = new Intent(this,addTravelActivity.class);
        intent.putExtra("user_id",u.getUser_id());
        intent.putExtra("classname",className);
        intent.putExtra("newAnnouncementid",newAnnouncementid);
        intent.putExtra("username",u.getName() + " " + u.getSurname());
        startActivityForResult(intent,REQUEST_CODE_ADD_TRAVEL);
    }

}