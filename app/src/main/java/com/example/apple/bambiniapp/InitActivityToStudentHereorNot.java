package com.example.apple.bambiniapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

public class InitActivityToStudentHereorNot extends AppCompatActivity {

    public static final int REQUEST_CODE_HERE = 1789;
    Spinner spinnerClass,spinnerClassHour;
    Button hereornotButton;
    String classname,classhour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init_to_student_hereor_not);

        spinnerClass = (Spinner) findViewById(R.id.spinnerClass);
        spinnerClassHour = (Spinner) findViewById(R.id.spinnerClassHours);
        hereornotButton = (Button) findViewById(R.id.hereornotbutton);

        hereornotButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                classname = spinnerClass.getSelectedItem().toString();
                classhour = spinnerClassHour.getSelectedItem().toString();
                gotoSwipe();

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.mystudents_menuitems, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.finishstudents:
                turnBackMainTeacher();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void turnBackMainTeacher(){
        Intent intent = new Intent(this, mainpageTeachersActivity.class);
        setResult(Activity.RESULT_OK,intent);
        finish();

    }

    public void gotoSwipe(){
        Intent intent = new Intent(this, StudentsInClassOrNotActivity.class);
        intent.putExtra("classname",classname);
        intent.putExtra("classhour",classhour);
        startActivityForResult(intent,REQUEST_CODE_HERE);

    }
}
