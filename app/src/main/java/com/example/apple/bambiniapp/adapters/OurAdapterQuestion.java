package com.example.apple.bambiniapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.apple.bambiniapp.R;
import com.example.apple.bambiniapp.entities.Question;

import java.util.ArrayList;

public class OurAdapterQuestion extends ArrayAdapter<Question> {

    ArrayList<Question> questionsList = new ArrayList<>();

    public OurAdapterQuestion(Context context, int textViewResourceId, ArrayList<Question> objects) {
        super(context, textViewResourceId, objects);
        questionsList = objects;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.layout_textviewforlistr, null);

        TextView textView = (TextView) v.findViewById(R.id.firstTextView);
        TextView textView2 = (TextView) v.findViewById(R.id.secondTextView);

        textView.setText(questionsList.get(position).getQuestion_title());
        textView2.setText(questionsList.get(position).getQuestion_date());

        return v;

    }
}
