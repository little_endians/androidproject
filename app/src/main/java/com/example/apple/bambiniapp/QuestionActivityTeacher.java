package com.example.apple.bambiniapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.apple.bambiniapp.adapters.adapterForOneQuestion;
import com.example.apple.bambiniapp.adapters.adapterForQuestionPage;
import com.example.apple.bambiniapp.entities.Question;
import com.example.apple.bambiniapp.entities.User;

import java.util.ArrayList;

public class QuestionActivityTeacher extends AppCompatActivity {

    static WhoSawAnnLoader WL;
    ListView questionInfoListT;
    ArrayList<Question> questionList = new ArrayList<Question>();
    String className,name = "";
    int position;
    int u_id;
    static User us;
    public static QuestionLoader QL;
    protected ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_teacher);
        final int user_id = getIntent().getIntExtra("user_id",0);
        name = getIntent().getStringExtra("name");

        u_id=user_id;
        className = getIntent().getStringExtra("className");
        position = getIntent().getIntExtra("position",0);
        longListing();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if(QL.questionList.isEmpty()){
            Log.i("QuestionLOADER-------", " THREAD IS CANCELED!!!! .....");
            return;
        }
        questionList = QL.questionList;
        for(Question q: questionList) {
            System.out.println("q is:  " + q.getQuestion_text());
        }

        questionInfoListT = (ListView) findViewById(R.id.oneQuestionListviewTeacher);
        adapterForOneQuestion adapterX = new adapterForOneQuestion(this,R.layout.layout_textviewfor1question,QL.questionList);
        questionInfoListT.setAdapter(adapterX);

        questionInfoListT.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                goToAnnouncement(view,position);
            }
        });


    }

    @Override
    protected void onStart() {

        if (new QuestionLoader().isCancelled()){
            Log.i("QuestionLOADER-------", "CANNOT START THE");
            return;
        }
        super.onStart();
        Log.i("TAGstart", "On Start .....");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("TAGstop", "On Stop .....");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("TAGdestroy", "On Destroy .....");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("TAGpause", "On Pause .....");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("TAGrestart", "On Restart .....");

    }
    protected void longListing(){
        mProgressDialog = ProgressDialog.show(this, "Please wait","Long operation starts...", true);
        new Thread() {
            @Override
            public void run() {
                QL  = new QuestionLoader(u_id);
                QL.execute();
                try {

                    // code runs in a thread
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mProgressDialog.dismiss();
                        }
                    });
                } catch (final Exception ex) {
                    Log.i("---","Exception in thread");
                }
            }
        }.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.mystudents_menuitems, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.finishstudents:
                turnBack();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void turnBack(){
        Intent intent = new Intent(this, mainpageTeachersActivity.class);
        setResult(Activity.RESULT_OK,intent);
        finish();

    }

    public void goToAnnouncement(View view,int position) {

        Intent intent = new Intent(this, AQuestionTeacherActivity.class);
        intent.putExtra("position",position);
        intent.putExtra("question",questionList.get(position));
        intent.putExtra("user_id",u_id);
        intent.putExtra("ntf_id",questionList.get(position).getQ_id());
        intent.putExtra("name",name);
        startActivityForResult(intent,1023);
    }
}
