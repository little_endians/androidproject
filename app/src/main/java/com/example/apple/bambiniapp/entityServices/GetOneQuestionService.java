package com.example.apple.bambiniapp.entityServices;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GetOneQuestionService {

    @GET("msorular/{id}")
    Call<ResponseBody> all(@Path("id") int id);

}

