package com.example.apple.bambiniapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.apple.bambiniapp.adapters.adapterForAnnouncementPage;
import com.example.apple.bambiniapp.entities.Announcement;
import com.example.apple.bambiniapp.entities.Comment;
import java.util.ArrayList;

import static com.example.apple.bambiniapp.mainpageParentsActivity.annAnswered;
import static com.example.apple.bambiniapp.mainpageParentsActivity.announcementsListP;

public class announcementParentActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_EDIT = 1071;
    public static final int REQUEST_CODE_COMMENT = 1907;
    ListView announcementInfoList;
    ArrayList<Object> toplam;
    ArrayList<Announcement> announcementP = new ArrayList<Announcement>();
    ArrayList<Comment> comments;
    adapterForAnnouncementPage adapterX;
    static CommentLoader CL ;
    String className,username= "";
    String pass;
    int position;
    int[] mustbeanswered,ianswered;
    int ntf_id,user_id;
    boolean iWrote,yanit = false;
    static String[] votedNow;
    private static boolean noteEdited = false;
    Comment c ;
    int commentTouch = 0;

    String editedTitle = " ",editedContent = " ",editedDate = " ",editedornot= " ";

    @Override
    protected void onCreate(final Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_announcement);

        className = getIntent().getStringExtra("className");
        position = getIntent().getIntExtra("position",0);
        username = getIntent().getStringExtra("username");
        pass = getIntent().getStringExtra("pass");
        user_id = getIntent().getIntExtra("user_id",0);
        yanit = getIntent().getBooleanExtra("yanit",yanit);
        mustbeanswered = getIntent().getIntArrayExtra("mustbeanswered");
        ianswered = getIntent().getIntArrayExtra("ianswered");

        announcementP.add(announcementsListP.get(position));
        Announcement answerTAG = new Announcement(0, "", "Cevap vermek için buraya basılı tutun.", "");
        announcementP.add(answerTAG);
        Announcement commentTAG = new Announcement(0, "", "                      YORUMLAR", "");
        announcementP.add(commentTAG);

        ntf_id = announcementsListP.get(position).getntf_id();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        comments =new ArrayList<>();

        announcementInfoList = (ListView) findViewById(R.id.oneAnnouncementListview);
        adapterX = new adapterForAnnouncementPage(this,R.layout.layout_textviewfor1announcement,announcementP);
        announcementInfoList.setAdapter(adapterX);

        announcementInfoList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View v, int position, long id){


                if(position == 2){
                    if(commentTouch == 0){
                    CL = new CommentLoader(ntf_id); // must be initialized here to do cont. tasks
                    CL.execute();

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    comments = CL.commentList;
                    ArrayList<Announcement> commentS = new ArrayList<Announcement>();

                    for (int i = 0; i<comments.size(); i++){
                        commentS.add(i,new Announcement(comments.get(i).getComment_id(),comments.get(i).getWriter_name() + " 'dan yorum :",comments.get(i).getComment(), " "));
                        Log.e("koment",commentS.get(0).getAnnouncement_content());
                    }

                    announcementP.addAll(commentS);
                    adapterX.notifyDataSetChanged();
                    commentTouch = commentTouch + 1;}
                }

            }
        });

        announcementInfoList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int arg2, long arg3) {

                if(arg2 == 1) {
                    createAndShowAlertDialog(position);
                }
                if(arg2 == 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(announcementParentActivity.this);
                    builder.setTitle("Yorum yapmak istiyor musunuz ?");
                    builder.setPositiveButton("EVET", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            commenting(position);
                            dialog.dismiss();
                        }
                    });
                    builder.setNegativeButton("HAYIR", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                return true;
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("TAGstart", "On Start .....");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("TAGstop", "On Stop .....");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("TAGdestroy", "On Destroy .....");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("TAGpause", "On Pause .....");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("TAGrestart", "On Restart .....");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("TAGresume", "On Resume .....");

        if(editedornot.equals("true") && !editedContent.equals(" ") && !editedTitle.equals(" ")){
            Log.e("WWWQQQQ","SSLSLSLSLSLSLSLLSLSL");
            Announcement ann = new Announcement(ntf_id,editedTitle,editedContent,editedDate);
            announcementsListP.set(position,ann);
            announcementP.remove(0);
            announcementP.add(ann);

            announcementInfoList = (ListView) findViewById(R.id.oneAnnouncementListview);
            adapterForAnnouncementPage adapterX = new adapterForAnnouncementPage(this,R.layout.layout_textviewfor1announcement,announcementP);
            announcementInfoList.setAdapter(adapterX);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuItem classNameItem;
        getMenuInflater().inflate(R.menu.actionbar_menuitems_ann_parent_noedit, menu);
        classNameItem = menu.findItem(R.id.action_class_name);
        classNameItem.setTitle(className);

        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (REQUEST_CODE_EDIT) : {

                if (resultCode == Activity.RESULT_OK) {

                    editedTitle = data.getStringExtra("editedTitle");
                    editedContent = data.getStringExtra("editedContent");
                    editedDate = data.getStringExtra("editedDate");
                    editedornot = data.getStringExtra("edited");
                    Log.e("WWWQQQQ",editedContent + editedornot + editedTitle);

                }
                break;
            }
            case (REQUEST_CODE_COMMENT ) : {

                if (resultCode == Activity.RESULT_OK) {
                    c = data.getParcelableExtra("addedComment");
                    Announcement an = new Announcement(c.getComment_id(),c.getComment_tag(),c.getComment(), " ");
                    announcementP.add(an);
                    adapterX.notifyDataSetChanged();
                }
                break;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_edit:
                noteEdited = true;
                Toast.makeText(this, "Edit note selected", Toast.LENGTH_SHORT)
                        .show();
                Intent intent = new Intent(this, editAnnouncementActivity.class);
                intent.putExtra("title",announcementsListP.get(position).getAnnouncement_tag());
                intent.putExtra("content",announcementsListP.get(position).getAnnouncement_content());
                intent.putExtra("date",announcementsListP.get(position).getAnnouncement_date());
                startActivityForResult(intent,REQUEST_CODE_EDIT);
                break;
            case android.R.id.home:

                onBackPressed();
                return true;

            default:
                break;
        }
        return true;
    }

    private void createAndShowAlertDialog(int position) {

        if(mustbeanswered[position]==1 && ianswered[position]==0 && annAnswered[position]==0){

            AlertDialog.Builder builder = new AlertDialog.Builder(announcementParentActivity.this);
            builder.setTitle("Cevabınız nedir ?");
            builder.setPositiveButton("EVET", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    AnswertoPoll anspoll = new AnswertoPoll();
                    Log.e("kamamammama",String.valueOf(user_id));
                    anspoll.postingAnswer(announcementP.get(0).getntf_id(),"evet",pass,user_id);
                    dialog.dismiss();
                }
            });
            builder.setNegativeButton("HAYIR", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    AnswertoPoll anspoll = new AnswertoPoll();
                    anspoll.postingAnswer(announcementP.get(0).getntf_id(),"hayir",pass,user_id);
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
            annAnswered[position] = 1;
        }
        else if((mustbeanswered[position]==1 && ianswered[position]==1)|| annAnswered[position]==1){
            AlertDialog.Builder builder = new AlertDialog.Builder(announcementParentActivity.this);
            builder.setTitle("Daha önce oy kullanmışsınız. ");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
        else if(mustbeanswered[position]==0){
            AlertDialog.Builder builder = new AlertDialog.Builder(announcementParentActivity.this);
            builder.setTitle("Bu duyuruya oy kullanılamaz. ");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }

    }

    public void commenting(int position){

        Intent intent = new Intent(this,CommentingActivity.class);
        intent.putExtra("user_id",user_id);
        intent.putExtra("username",username);
        intent.putExtra("ntf_id",announcementsListP.get(position).getntf_id());
        startActivityForResult(intent,REQUEST_CODE_COMMENT);
    }

}