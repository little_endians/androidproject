package com.example.apple.bambiniapp;

import android.os.AsyncTask;
import android.util.Log;

import com.example.apple.bambiniapp.entities.Announcement;
import com.example.apple.bambiniapp.entityServices.AnnouncementService;
import com.example.apple.bambiniapp.entityServices.AnnouncementWhoSawService;
import com.example.apple.bambiniapp.entityServices.StudentRequestsService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

public class StudentRequestsLoader extends AsyncTask<String, Response<ResponseBody>,Response<ResponseBody>> {

    int id ;

    public StudentRequestsLoader(int id) {
        this.id = id;
    }

    RetrofitConnection rc = new RetrofitConnection();
    Retrofit retrofit = rc.getRetrofit();

    Response<ResponseBody> response;
    ArrayList<String> studentRequestsList ;
    ArrayList<Integer> studentIDs;
    Boolean isPOST = false;

    @Override
    protected Response<ResponseBody> doInBackground(String... strings) {
        StudentRequestsService studentreqService = retrofit.create(StudentRequestsService.class);
        studentRequestsList = new ArrayList<String>();
        studentIDs = new ArrayList<Integer>();

        Call<ResponseBody> call = studentreqService.getStudentRequests();

        try {

            response = call.execute();

            if(!response.isSuccessful()){
                Log.e("RESPONSE STU. REQUESTS", "RESPONSE FAILED !");
                return null;
            }

            String jsonData = response.body().string();
            System.out.println("DO-IN-BACK STU. REQUESTS "+jsonData);
            JSONObject obj = new JSONObject(jsonData);
            JSONArray whosawlist =  obj.getJSONArray("ogrenci");

            for (int i = 0; i < whosawlist.length() ; i++) {
                JSONObject json_whosaw = new JSONObject(whosawlist.get(i).toString());
                String name = json_whosaw.getString("name") + " " + json_whosaw.getString("surname");
                studentRequestsList.add(name);
                Log.e("XXXZZZCCC", name);
                int id = json_whosaw.getInt("s_id");
                studentIDs.add(id);
                Log.e("XXXZZZCCC", String.valueOf(id));

            }

        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return response;
    }
    // Runs after doInBackground.
    @Override
    protected void onPostExecute(Response<ResponseBody> response) {

        if (response == null) {
            Log.e("RESPONSE onPostExecute", "...RESPONSE FAILED!...");
        }

    }

    // Runs in UI before background thread is called
    @Override
    protected void onPreExecute(){

    }


    // This is called from background thread but runs in UI
    @Override
    protected void onProgressUpdate(Response<ResponseBody>... values) {

        // Do things like update the progress bar
    }

}
