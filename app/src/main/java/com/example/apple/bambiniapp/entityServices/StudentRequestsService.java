package com.example.apple.bambiniapp.entityServices;

import com.example.apple.bambiniapp.dataModelsToCrud.PostCommentDatas;
import com.example.apple.bambiniapp.dataModelsToCrud.SendStudentRequests;
import com.example.apple.bambiniapp.dataModelsToCrud.StudentDatas;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface StudentRequestsService {

    @GET("mogrenciListesi")
    Call<ResponseBody> getStudentRequests();

    @POST("mogrenciOnay/{id}")
    Call<ResponseBody> postStudentRequests(@Path("id") int id, @Body SendStudentRequests data);
}
