package com.example.apple.bambiniapp;

import android.util.Log;

import com.example.apple.bambiniapp.dataModelsToCrud.EditAnnouncementDatas;
import com.example.apple.bambiniapp.entityServices.AnnouncementEditService;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class EditedAnnouncementPost  {

    RetrofitConnection rc = new RetrofitConnection();
    Retrofit retrofit = rc.getRetrofit();

    public void postingEditedAnn(int ntf_id, String title, String content, String creationdate) {

        AnnouncementEditService editService = retrofit.create(AnnouncementEditService.class);
        Call<ResponseBody> call = editService.create(ntf_id,new EditAnnouncementDatas(ntf_id,title, content, creationdate));

        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {

                    try {
                        Log.e("Ann post return", response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("FAIL", "onFailure ");

            }
        });

    }}