package com.example.apple.bambiniapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.apple.bambiniapp.R;

import java.util.ArrayList;

public class adapterForSlideMenu extends BaseAdapter {

    private ArrayList<String> panelItems;
    private TextView txt_title;
    private Context ctx;

    public adapterForSlideMenu(ArrayList<String> items, Context context) {
        this.panelItems = items;
        this.ctx = context;
    }

    @Override
    public int getCount() {
        return panelItems.size();
    }

    @Override
    public Object getItem(int position) {
        return panelItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        // Her list item için custom tasarım yüklüyor
        if (view == null) {

            view = LayoutInflater.from(ctx).inflate(R.layout.layout_textviewfor1slidemenuitem, null);

            // Yeni tasarım içindeki title textine ulaşıp, veriyi set ediyor
            txt_title = (TextView) view.findViewById(R.id.panel_textView);
            txt_title.setText(panelItems.get(position));

            return view;
        }

        return null;
    }


}
