package com.example.apple.bambiniapp;

import android.util.Log;

import com.example.apple.bambiniapp.dataModelsToCrud.PostCommentDatas;
import com.example.apple.bambiniapp.dataModelsToCrud.SendStudentRequests;
import com.example.apple.bambiniapp.entities.Comment;
import com.example.apple.bambiniapp.entityServices.CommentService;
import com.example.apple.bambiniapp.entityServices.HereorNotAcceptencePostService;
import com.example.apple.bambiniapp.entityServices.StudentRequestsService;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class HereorNotAcceptencePost {

    RetrofitConnection rc = new RetrofitConnection();
    Retrofit retrofit = rc.getRetrofit();

    public void sendAcceptence() {

        HereorNotAcceptencePostService hereService = retrofit.create(HereorNotAcceptencePostService.class);
        Call<ResponseBody> call = hereService.postAcceptanceStudents();

        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {

                    try {
                        Log.e("Ann post return COMMENT", response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("FAIL", "onFailure ");

            }
        });

    }
}
