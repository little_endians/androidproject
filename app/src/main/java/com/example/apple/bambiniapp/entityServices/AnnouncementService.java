package com.example.apple.bambiniapp.entityServices;

import com.example.apple.bambiniapp.dataModelsToCrud.NewAnnouncementDatas;
import com.example.apple.bambiniapp.entities.Announcement;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface AnnouncementService {

        @GET("mAnnouncements/{user_id}")
        Call<ResponseBody> all(@Path("user_id") int user_id);

        @POST("newAnn")
        Call<ResponseBody> create(@Body NewAnnouncementDatas data);

    }