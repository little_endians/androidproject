package com.example.apple.bambiniapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.example.apple.bambiniapp.adapters.adapterForHereOrNot;
import com.example.apple.bambiniapp.adapters.adapterStudentRequests;

import java.util.ArrayList;

public class StudentHereActivity extends AppCompatActivity {

    static MyStudentSituationLoader MSH ;
    static ArrayList<String> dersler = new ArrayList<>();
    public static ArrayList<String> tarih = new ArrayList<>();
    adapterForHereOrNot adapterMSH;
    ListView ourList;
    int student_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_here);

        student_id = getIntent().getIntExtra("student_id",0);

        MSH = new MyStudentSituationLoader(student_id);
        MSH.execute();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        dersler = MSH.derssaati;
        tarih = MSH.tarihler;

        ourList = (ListView) findViewById(R.id.myStudentsHereListview);
        adapterMSH = new adapterForHereOrNot(this,R.layout.layout_textviewforlistr,dersler);
        ourList.setAdapter(adapterMSH);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.mystudents_menuitems, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.finishstudents:
                Intent intent = new Intent(this, MyStudentsActivity.class);
                setResult(Activity.RESULT_OK,intent);
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
