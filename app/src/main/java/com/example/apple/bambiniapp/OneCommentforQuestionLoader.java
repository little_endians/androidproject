package com.example.apple.bambiniapp;

import android.os.AsyncTask;
import android.util.Log;
import com.example.apple.bambiniapp.entities.Comment;
import com.example.apple.bambiniapp.entityServices.GetOneQuestionService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

public class OneCommentforQuestionLoader extends AsyncTask<String, Response<ResponseBody>,Response<ResponseBody>> {

    int id;

    public OneCommentforQuestionLoader(int id) {
        this.id = id;
    }

    RetrofitConnection rc = new RetrofitConnection();
    Retrofit retrofit = rc.getRetrofit();
    Response<ResponseBody> response;
    ArrayList<Comment> commentList ;
    Comment cmm;

    @Override
    protected Response<ResponseBody> doInBackground(String... strings) {
        GetOneQuestionService commentService = retrofit.create(GetOneQuestionService.class);
        commentList = new ArrayList<Comment>();
        cmm = new Comment();

        Call<ResponseBody> call = commentService.all(id);

        try {

            response = call.execute();

            if(!response.isSuccessful()){
                Log.e("RESPONSE FOR LOGIN", "...RESPONSE FAILED!...");
                return null;
            }

            String jsonData = response.body().string();
            System.out.println("DO-IN-BACK COMMENT-- "+jsonData);
            JSONObject obj = new JSONObject(jsonData);
            //JSONArray cmt =  obj.getJSONArray("comment");
            JSONObject cmt =  obj.getJSONObject("comment");

            if(!cmt.getString("cmmt_box").equals("")) {
                String date = cmt.getString("cmmt_creationdate");
                date = date.substring(0, date.indexOf("T"));
                cmm = new Comment(cmt.getInt("c_id"), cmt.getString("cmmt_box"), cmt.getString("cmmt_tag"), date, cmt.getString("writer_name"));
            }

        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return response;
    }
    // Runs after doInBackground.
    @Override
    protected void onPostExecute(Response<ResponseBody> response) {

        if (response == null) {
            Log.e("RESPONSE onPostExecute", "...RESPONSE FAILED!...");
        }

    }

    // Runs in UI before background thread is called
    @Override
    protected void onPreExecute(){

    }


    // This is called from background thread but runs in UI
    @Override
    protected void onProgressUpdate(Response<ResponseBody>... values) {

        // Do things like update the progress bar
    }

}


