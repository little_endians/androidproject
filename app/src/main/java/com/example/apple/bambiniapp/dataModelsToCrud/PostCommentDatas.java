package com.example.apple.bambiniapp.dataModelsToCrud;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostCommentDatas {

    @SerializedName("ntf_id")
    @Expose
    private int ntf_id;

    @SerializedName("user_id")
    @Expose
    private int user_id;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("text")
    @Expose
    private String content;

    @SerializedName("date")
    @Expose
    private String creationdate;

    @SerializedName("name")
    @Expose
    private String writer_name;

    public PostCommentDatas(int ntf_id, int user_id, String title, String content, String creationdate,String writer_name) {
        this.ntf_id = ntf_id;
        this.user_id = user_id;
        this.title = title;
        this.content = content;
        this.creationdate = creationdate;
        this.writer_name = writer_name;
    }
}
