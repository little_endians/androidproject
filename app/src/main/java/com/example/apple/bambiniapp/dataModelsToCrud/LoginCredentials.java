package com.example.apple.bambiniapp.dataModelsToCrud;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginCredentials {

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("password")
    @Expose
    private String password;

    public LoginCredentials(String email, String password) {
        this.email = email;
        this.password = password;
    }

}