package com.example.apple.bambiniapp.dataModelsToCrud;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendQuestion {

    @SerializedName("user_id")
    @Expose
    private int user_id;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("teacher")
    @Expose
    private int teacher_id;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("text")
    @Expose
    private String text;

    @SerializedName("date")
    @Expose
    private String date;

    public SendQuestion(int user_id, String username, int teacher_id, String title, String text,String date) {
        this.user_id = user_id;
        this.username = username;
        this.teacher_id = teacher_id;
        this.title = title;
        this.text = text;
        this.date = date;
    }

}
