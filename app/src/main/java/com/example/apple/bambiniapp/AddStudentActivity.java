package com.example.apple.bambiniapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AddStudentActivity extends AppCompatActivity {

    String s_name,s_surname,s_classname;
    EditText nameT,surnameT,classT;
    Button addT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);

        nameT = (EditText) findViewById(R.id.studentName);
        surnameT = (EditText) findViewById(R.id.studentSurname);
        classT = (EditText) findViewById(R.id.studentClassname);

        addT = (Button) findViewById(R.id.studentAdd);

        addT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                s_name = nameT.getText().toString();
                s_surname = surnameT.getText().toString();
                s_classname = classT.getText().toString();

                StudentCrudOps spost = new StudentCrudOps();
                spost.postingQuestion(s_name,s_surname,s_classname,getIntent().getIntExtra("user_id",102));
                sendAnnGoBack(view);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        getMenuInflater().inflate(R.menu.addstudent_menuitems, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void sendAnnGoBack(View view) {

        Intent intent = new Intent(this, mainpageParentsActivity.class);
        setResult(Activity.RESULT_OK,intent);
        finish();

    }
}
