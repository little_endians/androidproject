package com.example.apple.bambiniapp.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User implements Parcelable {

    @SerializedName("user_id")
    @Expose
    private int user_id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("surname")
    @Expose
    private String surname;

    @SerializedName("phonenumber")
    @Expose
    private String phone;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("password")
    @Expose
    private String password;

    private Boolean teacher;

    public User(int user_id, String name, String surname, String phone,String email,String pass){

        this.user_id = user_id;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.email = email;
        this.password = pass;
    }
    public User(){

        this.user_id = 0;
        this.name = null;
        this.surname = null;
        this.phone = null;
        this.email = null;
        this.password = null;
    }

    public String getEmail() { return email; }

    public String getName() { return name; }

    public String getPasswd() { return password; }

    public String getPhone() { return phone; }

    public String getSurname() { return surname; }

    public int getUser_id() { return user_id; }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPasswd(String password) {
        this.password = password;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setUser_id(int user_id) { this.user_id = user_id; }

    public Boolean getTeacher() {
        return teacher;
    }

    public void setTeacher(Boolean teacher) {
        this.teacher = teacher;
    }

    public User(Parcel in){
        String[] data= new String[6];

        in.readStringArray(data);

        this.user_id = Integer.parseInt(data[0]);
        this.name = data[1];
        this.surname = data[2];
        this.phone = data[3];
        this.email = data[4];
        this.password = data[5];

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeStringArray(new String[]{String.valueOf(this.user_id),this.name,this.surname,this.phone,this.email,this.password});
    }

    public static final Parcelable.Creator<User> CREATOR= new Parcelable.Creator<User>() {

        @Override
        public User createFromParcel(Parcel source) {

            return new User(source);  //using parcelable constructor
        }

        @Override
        public User[] newArray(int size) {

            return new User[size];
        }
    };

    @Override
    public String toString() {
        return  "user_id='" + user_id + '\'' +
                ", name='" + name + '\'' +
                ", surname=" + surname +
                ", phone=" + phone +
                ", email='" + email + '\'' +
                ", pass=" + password +
                '}';
    }

}