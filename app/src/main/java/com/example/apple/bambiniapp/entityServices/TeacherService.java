package com.example.apple.bambiniapp.entityServices;

import com.example.apple.bambiniapp.entities.User;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface TeacherService {
    @GET("and")
    Call<List<User>> all();

    @GET("user/{user_id}")
    Call<User> get(@Path("user_id") int u_id);

    @POST("msignupTeacher")
    Call<ResponseBody> create(@Body User user);
}

