package com.example.apple.bambiniapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.apple.bambiniapp.R;
import com.example.apple.bambiniapp.entities.Announcement;

import java.util.ArrayList;

import static com.example.apple.bambiniapp.StudentHereActivity.tarih;

public class adapterStudentRequests extends ArrayAdapter<String> {

    ArrayList<String> studentRequestList;

    public adapterStudentRequests(Context context, int textViewResourceId, ArrayList<String> objects) {
        super(context, textViewResourceId, objects);
        studentRequestList = objects;
    }

    @Override
    public int getCount() {
        return  studentRequestList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.layout_textviewforlistr, null);

        TextView textView = (TextView) v.findViewById(R.id.firstTextView);
        TextView textView2 = (TextView) v.findViewById(R.id.secondTextView);

        textView.setText(studentRequestList.get(position).toString());
        textView2.setText(" ");

        return v;

    }

}