package com.example.apple.bambiniapp.entityServices;

import com.example.apple.bambiniapp.dataModelsToCrud.NewAnnouncementDatas;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface SpecialAnnouncementService {

    @POST("mozelDuyuru")
    Call<ResponseBody> create(@Body NewAnnouncementDatas data);
}
