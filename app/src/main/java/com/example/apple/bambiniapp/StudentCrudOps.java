package com.example.apple.bambiniapp;

import android.util.Log;

import com.example.apple.bambiniapp.dataModelsToCrud.SendStudentRequests;
import com.example.apple.bambiniapp.dataModelsToCrud.StudentDatas;
import com.example.apple.bambiniapp.entityServices.StudentAddService;
import com.example.apple.bambiniapp.entityServices.StudentRequestsService;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class StudentCrudOps {

    RetrofitConnection rc = new RetrofitConnection();
    Retrofit retrofit = rc.getRetrofit();

    public void postingQuestion(String name, String surname, String classname,int user_id) {

        StudentAddService addService = retrofit.create(StudentAddService.class);
        Call<ResponseBody> call = addService.addStudent(new StudentDatas(name,surname,classname,user_id));

        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {

                    try {
                        Log.e("Ann post returnSS", response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("FAIL", "onFailure ");

            }
        });

    }

    public void postingRequest(String onay, String red, int student_id) {

        StudentRequestsService reqService = retrofit.create(StudentRequestsService.class);
        Call<ResponseBody> call = reqService.postStudentRequests(student_id,new SendStudentRequests(onay,red,student_id));

        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {

                    try {
                        Log.e("Ann post returnMM", response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("FAIL", "onFailure ");

            }
        });

    }
}
