package com.example.apple.bambiniapp;

import android.util.Log;

import com.example.apple.bambiniapp.dataModelsToCrud.NewAnnouncementDatas;
import com.example.apple.bambiniapp.entityServices.AnnouncementService;
import com.example.apple.bambiniapp.entityServices.SpecialAnnouncementService;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class NewAnnouncementPost  {

    RetrofitConnection rc = new RetrofitConnection();
    Retrofit retrofit = rc.getRetrofit();

    public void postingAnn(int user_id, String title, String content, String creationdate, String classname, String type) {

        AnnouncementService annService = retrofit.create(AnnouncementService.class);
        Call<ResponseBody> call = annService.create(new NewAnnouncementDatas(user_id, title, content, creationdate, classname,type));

        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {

                    try {
                        Log.e("Ann post return", response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("FAIL", "onFailure ");

            }
        });

    }

    public void postingSpeAnn(String tarih, String baslangic, String bitis, String konum, int user_id, String username, String tur, String date) {

        SpecialAnnouncementService annService = retrofit.create(SpecialAnnouncementService.class);
        Call<ResponseBody> call = annService.create(new NewAnnouncementDatas(tarih,baslangic,bitis,konum,user_id,username,tur,date));

        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {

                    try {
                        Log.e("Ann post return", response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("FAIL", "onFailure ");

            }
        });

    }
}