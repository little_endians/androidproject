package com.example.apple.bambiniapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.apple.bambiniapp.entities.Announcement;
import com.example.apple.bambiniapp.entities.Comment;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CommentingActivity extends AppCompatActivity {

    EditText commentText;
    Button commentButton;

    String commentC,creationdate,username;
    int user_id,ntf_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commenting);

        username = getIntent().getStringExtra("username");
        user_id = getIntent().getIntExtra("user_id",102);
        ntf_id = getIntent().getIntExtra("ntf_id",0);

        commentText = (EditText) findViewById(R.id.commentText);
        commentButton = (Button) findViewById(R.id.commentButton);

        commentC = commentText.getText().toString();

        commentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                commentC = commentText.getText().toString();

                creationdate = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime());
                user_id = getIntent().getIntExtra("user_id",23);
                username = getIntent().getStringExtra("username");

                PostCommentOperation commentPost = new PostCommentOperation();
                commentPost.postingComment(ntf_id,user_id,username + " kişisinden yorum:",commentC,creationdate,username);
                Toast.makeText(getApplicationContext(), "Yorum eklendi ...",Toast.LENGTH_SHORT).show();
                sendAnnGoBack(view);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        getMenuInflater().inflate(R.menu.actionbar_manuitems_comment, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void sendAnnGoBack(View view) {

        Intent intent = new Intent(this, announcementParentActivity.class);
        Comment comment = new Comment(0,commentC,username+" kisisinden yorum:",creationdate,username);
        intent.putExtra("addedComment",comment);
        setResult(Activity.RESULT_OK,intent);
        finish();


    }
}
