package com.example.apple.bambiniapp;

import android.os.AsyncTask;
import android.util.Log;

import com.example.apple.bambiniapp.entityServices.MyStudentHereService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MyStudentSituationLoader extends AsyncTask<String, Response<ResponseBody>,Response<ResponseBody>> {

    int id ;

    public MyStudentSituationLoader(int id) {
        this.id = id;
    }

    RetrofitConnection rc = new RetrofitConnection();
    Retrofit retrofit = rc.getRetrofit();

    Response<ResponseBody> response;
    ArrayList<String> derssaati ;
    ArrayList<String> tarihler ;

    @Override
    protected Response<ResponseBody> doInBackground(String... strings) {
        MyStudentHereService mshService = retrofit.create(MyStudentHereService.class);
        derssaati = new ArrayList<String>();
        tarihler = new ArrayList<String>();

        Call<ResponseBody> call = mshService.getmystudentHere(id);

        try {

            response = call.execute();

            if(!response.isSuccessful()){
                Log.e("RESPONSE S HERE REQUEST", "RESPONSE FAILED !");
                return null;
            }

            String jsonData = response.body().string();
            System.out.println("DO-IN-BACK S HERE REQUESTS "+jsonData);
            JSONObject obj = new JSONObject(jsonData);
            JSONArray whosawlist =  obj.getJSONArray("ogrenciDev");

            for (int i = 0; i < whosawlist.length() ; i++) {
                JSONObject json_whosaw = new JSONObject(whosawlist.get(i).toString());
                String ds = json_whosaw.getString("derssaati");
                ds = ds.concat(" yok .");
                derssaati.add(ds);
                String tarih = json_whosaw.getString("tarih");
                tarih = tarih.substring(0,tarih.indexOf("T"));
                tarihler.add(tarih);

            }

        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return response;
    }
    // Runs after doInBackground.
    @Override
    protected void onPostExecute(Response<ResponseBody> response) {

        if (response == null) {
            Log.e("RESPONSE onPostExecute", "...RESPONSE FAILED!...");
        }

    }

    // Runs in UI before background thread is called
    @Override
    protected void onPreExecute(){

    }


    // This is called from background thread but runs in UI
    @Override
    protected void onProgressUpdate(Response<ResponseBody>... values) {

        // Do things like update the progress bar
    }

}
