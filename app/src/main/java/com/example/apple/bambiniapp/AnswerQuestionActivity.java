package com.example.apple.bambiniapp;

import android.app.Activity;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.apple.bambiniapp.entities.Comment;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AnswerQuestionActivity extends AppCompatActivity {

    EditText questionanswer;
    Button questionanswerb;
    static String answer,creationdate;
    static Comment c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer_question);

        questionanswer = (EditText)findViewById(R.id.questionanswer);
        questionanswerb = (Button) findViewById(R.id.questionanswerb);

        questionanswerb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                creationdate = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime());
                answer = questionanswer.getText().toString();
                PostCommentOperation cmm = new PostCommentOperation();
                cmm.postingComment(getIntent().getIntExtra("qid",0),getIntent().getIntExtra("user_id",0),"",answer,creationdate,getIntent().getStringExtra("name"));
                c = new Comment(getIntent().getIntExtra("qid",0),answer,"",creationdate,"");
                turnBack();

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.mystudents_menuitems, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.finishstudents:
                turnBack();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void turnBack(){
        Intent intent = new Intent(this, AQuestionTeacherActivity.class);
        Log.e("ccccreyiz",c.getComment());
        intent.putExtra("c",c);
        setResult(Activity.RESULT_OK,intent);
        finish();

    }
}
