package com.example.apple.bambiniapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.apple.bambiniapp.adapters.OurAdapter;
import com.example.apple.bambiniapp.adapters.adapterForSlideMenu;
import com.example.apple.bambiniapp.entities.Announcement;
import com.example.apple.bambiniapp.entities.User;

import java.util.ArrayList;

public class mainpageParentsActivity extends AppCompatActivity {

    ListView ourList;
    ListView panelList;
    public static final int REQUEST_CODE_ADD = 201;
    public static final int REQUEST_CODE_List = 301;
    public static final int REQUEST_CODE_ADDSTUDENT = 1919;
    public static final int REQUEST_CODE_MYSTUDENTS = 1980;
    public static final int  REQUEST_CODE_Listpq = 669;
    private static AnnouncementLoader AL ;

    static boolean firstTime = true;
    static  ArrayList<Announcement> announcementsListP = new ArrayList<>();

    static ArrayList<String> panelItems = new ArrayList<>();
    static User u;

    int annsSize,answered = 0;
    int[] mustbeAnswered,ianswered;

    boolean yanit = false;

    String className = "3-A";
    DrawerLayout drawerLayout;

    private static boolean aquestionAsked  = false;
    static int annAnswered[];


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        Log.e("YYYYYYY","PPPPPPPP");
        savedInstanceState.putParcelable("user", u);
        savedInstanceState.putParcelableArrayList("announcementsListP",announcementsListP);
        super.onSaveInstanceState(savedInstanceState);
    }


    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.e("PPPPPP","YYYYYY");
        super.onRestoreInstanceState(savedInstanceState);

        u = savedInstanceState.getParcelable("user");
        announcementsListP = savedInstanceState.getParcelableArrayList("announcementsListP");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainpage_parents);



        if(savedInstanceState == null) {
            if (firstTime){
                setPanelItems();
                u = getIntent().getParcelableExtra("user");
                questionActivitiy.setUs(u);
                Log.e("Useer", u.toString());
                firstTime=false;

            }


            AL = new AnnouncementLoader(u.getUser_id()); // must be initialized here to do cont. tasks
            AL.execute();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            announcementsListP = AL.announcementList;
        }

        ourList = (ListView) findViewById(R.id.announcementsListviewP);
        OurAdapter adapterX = new OurAdapter(this,R.layout.layout_textviewforlistr,announcementsListP);
        ourList.setAdapter(adapterX);

        annsSize = announcementsListP.size();
        mustbeAnswered = new int[annsSize];
        ianswered = new int[annsSize];
        annAnswered = new int[annsSize];

        for(int i = 0; i < annsSize; i++){

            if(announcementsListP.get(i).getType().equals("yanit")){
                mustbeAnswered[i] = 1;
            }

            if(announcementsListP.get(i).getEvet().contains(String.valueOf(u.getUser_id())) || announcementsListP.get(i).getHayir().contains(String.valueOf(u.getUser_id())) ){
                ianswered[i] = 1;
            }
        }

        ourList.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,long id) {


                goToAnnouncement(view,position,answered);
            }
        });

        panelList = (ListView) findViewById(R.id.panelItemsListviewP);
        adapterForSlideMenu adapter = new adapterForSlideMenu(panelItems, getApplicationContext());
        panelList.setAdapter(adapter);

        panelList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                test(view,position);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        MenuItem classNameItem;
        getMenuInflater().inflate(R.menu.actionbar_menuitemsparent, menu);

        classNameItem = menu.findItem(R.id.action_class_nameP);
        classNameItem.setTitle(className);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_ask:
                Intent intent = new Intent(this,askQuestionActivity.class);
                intent.putExtra("user_id",u.getUser_id());
                intent.putExtra("username",u.getName() + " " + u.getSurname());
                startActivity(intent);
                break;
            case R.id.action_class_name:
                Toast.makeText(this, "Class-name is clicked", Toast.LENGTH_SHORT)
                        .show();
                break;
            default:
                break;
        }

        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("TAGstart", "On Start .....");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("TAGstop", "On Stop .....");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("TAGdestroy", "On Destroy .....");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("TAGpause", "On Pause .....");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("TAGrestart", "On Restart .....");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("TAGresume", "On Resume .....");
    }

    public void goToAnnouncement(View view,int position,int answered) {

        Intent intent = new Intent(this, announcementParentActivity.class);
        intent.putExtra("class_name", className);
        intent.putExtra("position",position);
        intent.putExtra("answered",answered);
        intent.putExtra("yanit",yanit);
        intent.putExtra("username",u.getName() + " " + u.getSurname());
        intent.putExtra("pass",u.getPasswd());
        intent.putExtra("mustbeanswered",mustbeAnswered);
        intent.putExtra("ianswered",ianswered);
        intent.putExtra("user_id",u.getUser_id());
        startActivity(intent);

    }

    public void setPanelItems() {

        panelItems.add("Öğrenci Ekle");
        panelItems.add("Sorularım");
        panelItems.add("Öğrencilerim/Yoklama");

    }

    public void test(View view, int position)  {

        if(position == 0) {

            Intent intent = new Intent(this,AddStudentActivity.class);
            intent.putExtra("user_id",u.getUser_id());
            startActivityForResult(intent,REQUEST_CODE_ADDSTUDENT);
        }
        if(position == 1 ){

            Toast.makeText(this, "Yönlendiriliyor... ", Toast.LENGTH_SHORT)
                    .show();
            Intent intent = new Intent(this,questionActivitiy.class);
            intent.putExtra("user_id",u.getUser_id());
            intent.putExtra("classname",className);
            startActivityForResult(intent,REQUEST_CODE_List);

        }
        else{

        }

        if(position == 2) {

            Intent intent = new Intent(this,MyStudentsActivity.class);
            intent.putExtra("user_id",u.getUser_id());
            startActivityForResult(intent,REQUEST_CODE_MYSTUDENTS);
        }
        if(position == 3) {

            Toast.makeText(this, "Yönlendiriliyor... ", Toast.LENGTH_SHORT)
                    .show();
            Intent intent = new Intent(this, askquestionForParents.class);
            intent.putExtra("user_id", u.getUser_id());
            intent.putExtra("classname", className);
            startActivityForResult(intent, REQUEST_CODE_Listpq);
        }

    }

    public static void setFirstTime(boolean f) {
        mainpageParentsActivity.firstTime = f;
    }
}