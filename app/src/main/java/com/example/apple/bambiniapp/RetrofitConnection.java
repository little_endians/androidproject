package com.example.apple.bambiniapp;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitConnection {
    static Retrofit retrofit;
    static OkHttpClient client;

    public Retrofit getRetrofit(){

        client = new OkHttpClient.Builder()
                .connectTimeout(150, TimeUnit.SECONDS)
                .readTimeout(150,TimeUnit.SECONDS).build();
        retrofit = new Retrofit.Builder()
                .baseUrl("https://bambini.herokuapp.com").client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }

}
