package com.example.apple.bambiniapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.apple.bambiniapp.R;
import com.example.apple.bambiniapp.entities.Announcement;

import java.util.ArrayList;

public class adapterForAnnouncementPage extends ArrayAdapter<Announcement>  {

    ArrayList<Announcement> announcementsList = new ArrayList<>();

    public adapterForAnnouncementPage(Context context, int textViewResourceId, ArrayList<Announcement> objects) {
        super(context, textViewResourceId, objects);
        announcementsList = objects;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.layout_textviewfor1announcement, null);

        TextView textView = (TextView) v.findViewById(R.id.tagTextView);
        TextView textView2 = (TextView) v.findViewById(R.id.pollingAnnouncementTextView);
        TextView textView3 = (TextView) v.findViewById(R.id.dateTextView);

        textView.setText(announcementsList.get(position).getAnnouncement_tag());
        textView2.setText(announcementsList.get(position).getAnnouncement_content());
        textView3.setText(announcementsList.get(position).getAnnouncement_date());

        return v;

    }

}

