package com.example.apple.bambiniapp.dataModelsToCrud;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EditAnnouncementDatas {

    @SerializedName("ntf_id")
    @Expose
    private int ntf_id;

    @SerializedName("user_id")
    @Expose
    private int user_id;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("text")
    @Expose
    private String content;

    @SerializedName("date")
    @Expose
    private String creationdate;

    public EditAnnouncementDatas(int ntf_id,String title, String content, String creationdate) {
        this.ntf_id = ntf_id;
        this.title = title;
        this.content = content;
        this.creationdate = creationdate;
    }
}
