package com.example.apple.bambiniapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.apple.bambiniapp.adapters.adapterForQuestionPage;
import com.example.apple.bambiniapp.entities.Comment;
import com.example.apple.bambiniapp.entities.Question;

import java.util.ArrayList;

import static com.example.apple.bambiniapp.AnswerQuestionActivity.answer;
import static com.example.apple.bambiniapp.AnswerQuestionActivity.creationdate;

public class AQuestionTeacherActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_XXX= 804;

    ArrayList<Question> questionT = new ArrayList<Question>();
    ArrayList<Comment> commentT = new ArrayList<Comment>();
    Comment commentteacher;
    Question q = new Question();
    ListView oneQuestionListviewTeacher;
    adapterForQuestionPage adapterX;
    OneCommentforQuestionLoader cdf;
    int user_id,qid;
    static int oneClickComment = 0;
    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aquestion_teacher);

        q = getIntent().getParcelableExtra("question");
        questionT.add(q);

        Question answerTAG = new Question(0,0, "",0, "Verdiğiniz cevap için tıklayın.", "","","");
        questionT.add(answerTAG);

        oneQuestionListviewTeacher = (ListView) findViewById(R.id.oneQuestionListviewTeacher);
        adapterX = new adapterForQuestionPage(this,R.layout.layout_textviewfor1announcement,questionT);
        oneQuestionListviewTeacher.setAdapter(adapterX);

        oneQuestionListviewTeacher.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View v, int position, long id){

                if(position == 1 && oneClickComment==0){

                    user_id = getIntent().getIntExtra("user_id",0);
                    name = getIntent().getStringExtra("name");
                    qid = getIntent().getIntExtra("ntf_id",0);
                    cdf = new OneCommentforQuestionLoader(qid);
                    cdf.execute();

                    try {
                        Thread.sleep(1800);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    commentteacher = cdf.cmm;

                    if(commentteacher.getComment()!=null && oneClickComment == 0) {
                        Question q = new Question(0, 0, "", 0, commentteacher.getComment(), commentteacher.getComment_creationdate(), "", "");
                        questionT.add(q);
                        adapterX.notifyDataSetChanged();
                        oneClickComment++;
                    }else
                        Toast.makeText(getApplicationContext(),
                                "Cevap vermemişsiniz .",Toast.LENGTH_SHORT).show();

                }

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.actionbar_menuitems_teacher, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_maingeri:
                turnBack();
                return true;
            case R.id.action_cevapla:
                    gotoAnswer();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void turnBack(){
        Intent intent = new Intent(this, QuestionActivityTeacher.class);
        setResult(Activity.RESULT_OK,intent);
        finish();

    }

    public void gotoAnswer(){
        Intent intent = new Intent(this, AnswerQuestionActivity.class);
        intent.putExtra("qid",qid);
        intent.putExtra("user_id",user_id);
        intent.putExtra("name",name);
        setResult(Activity.RESULT_OK,intent);
        startActivityForResult(intent,REQUEST_CODE_XXX);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (REQUEST_CODE_XXX) : {

                if (resultCode == Activity.RESULT_OK ) {
                    Question qx = new Question(0,0,"",0,answer,creationdate,"","");
                    questionT.add(qx);
                    adapterX.notifyDataSetChanged();
                }
                break;
            }

        }
    }
}
