package com.example.apple.bambiniapp;

import android.os.AsyncTask;
import android.util.Log;

import com.example.apple.bambiniapp.dataModelsToCrud.LoginCredentials;
import com.example.apple.bambiniapp.entities.User;
import com.example.apple.bambiniapp.entityServices.LoginService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

// Here is the AsyncTask class:
//
// AsyncTask<Params, Progress, Result>.
//    Params – the type (Object/primitive) you pass to the AsyncTask from .execute()
//    Progress – the type that gets passed to onProgressUpdate()
//    Result – the type returns from doInBackground()
// Any of them can be String, Integer, Void, etc.

public class AuthenticationCheck extends AsyncTask<String,Response<ResponseBody>,Response<ResponseBody>> {

    RetrofitConnection rc = new RetrofitConnection();
    Retrofit retrofit = rc.getRetrofit();
    static boolean check = false;
    static User u = new User();
    Response<ResponseBody> response;
    static String[] s = new String[2];

    // This is run in a background thread
    @Override
     protected Response<ResponseBody> doInBackground(String[] log) {

        LoginService loginService = retrofit.create(LoginService.class);
        String email = s[0],password = s[1];
        Call<ResponseBody> call = loginService.loginWithCredentials(new LoginCredentials(email, password));

        try {

            response = call.execute();

            if(!response.isSuccessful()){
                Log.e("RESPONSE FOR LOGIN", "...RESPONSE FAILED!...");
                return null;
            }
            this.check = true;

            System.out.println("DO-IN-BACK--- "+response.body().toString());
            String jsonData = response.body().string();
            JSONObject obj = new JSONObject(jsonData);

            if(jsonData.contains("true"))
                u.setTeacher(true);
            else
                u.setTeacher(false);

            final JSONObject userObj =  obj.getJSONObject("user");
            u.setSurname(userObj.getString("surname"));
            u.setPhone(userObj.getString("phonenumber"));
            u.setUser_id(userObj.getInt("user_id"));
            u.setName(userObj.getString("name"));
            u.setEmail(userObj.getString("email"));
            u.setPasswd(userObj.getString("password"));

            System.out.println("User    <<<<<"+obj.getJSONObject("user"));


        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return response;
    }

    // Runs after doInBackground.
    @Override
    protected void onPostExecute(Response<ResponseBody> response) {

        if (response == null) {
            Log.e("RESPONSE onPostExecute", "...RESPONSE FAILED!...");
        }else if (getCheck() == false) {
            Log.e("check onPostExecute", "... check is FALSE!...");
        }
        this.cancel(true);

    }

    // Runs in UI before background thread is called
    @Override
    protected void onPreExecute(){

    }


    // This is called from background thread but runs in UI
    @Override
    protected void onProgressUpdate(Response<ResponseBody>... values) {

        // Do things like update the progress bar
    }

    public boolean getCheck(){
        return check;
    }

    public static void setS(String[] s) {
        AuthenticationCheck.s = s;
    }



}
