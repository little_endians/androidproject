package com.example.apple.bambiniapp;

import android.util.Log;

import com.example.apple.bambiniapp.dataModelsToCrud.NewAnnouncementDatas;
import com.example.apple.bambiniapp.dataModelsToCrud.answerPollAnnouncement;
import com.example.apple.bambiniapp.entityServices.AnnouncementService;
import com.example.apple.bambiniapp.entityServices.AnswerPollService;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AnswertoPoll {

    RetrofitConnection rc = new RetrofitConnection();
    Retrofit retrofit = rc.getRetrofit();


    public void postingAnswer(int id, String answer, String pass, int user_id) {

        AnswerPollService answerPollService = retrofit.create(AnswerPollService.class);

        Call<ResponseBody> call = answerPollService.post(id,new answerPollAnnouncement(user_id, answer, pass));
        final String pass2 = pass;
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {

                    try {
                        Log.e("Ann post return", response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("FAIL", "onFailure ");

            }
        });

    }
}
