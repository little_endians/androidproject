package com.example.apple.bambiniapp.entityServices;

import com.example.apple.bambiniapp.dataModelsToCrud.LoginCredentials;
import com.example.apple.bambiniapp.dataModelsToCrud.answerPollAnnouncement;
import com.example.apple.bambiniapp.entities.User;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface AnswerPollService {

    @POST("myanitDuyuru/{id}")
    Call<ResponseBody> post(@Path("id") int id,@Body answerPollAnnouncement data);
}
