package com.example.apple.bambiniapp.entityServices;

import com.example.apple.bambiniapp.entities.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface UserService {

    @POST("mlogin")
    Call<List<User>> all();

    @GET("user/{user_id}")
    Call<User> get(@Path("user_id") int u_id);

}