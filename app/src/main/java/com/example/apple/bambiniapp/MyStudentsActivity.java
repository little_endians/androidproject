package com.example.apple.bambiniapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.apple.bambiniapp.adapters.adapterStudentRequests;

import java.util.ArrayList;

public class MyStudentsActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_MYSTUDENTS_HERE = 1980;

    static MyStudentsLoader MSL ;
    static ArrayList<String> mystudentsList = new ArrayList<>();
    static ArrayList<Integer> mystudentsIDsList = new ArrayList<>();
    adapterStudentRequests adapterMS;
    ListView ourList;

    int user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_students);

        user_id = getIntent().getIntExtra("user_id",0);

        MSL = new MyStudentsLoader(user_id);
        MSL.execute();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        mystudentsList = MSL.myStudents;
        mystudentsIDsList = MSL.myStudentIDs;
        //Log.e("my students list 0.",studentsList.get(0));
        ourList = (ListView) findViewById(R.id.myStudentsListview);
        adapterMS = new adapterStudentRequests(this,R.layout.layout_textviewforlistr,mystudentsList);
        ourList.setAdapter(adapterMS);

        ourList.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                goToStudent(view,position);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        MenuItem classNameItem;
        getMenuInflater().inflate(R.menu.mystudents_menuitems, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.finishstudents:
                Intent intent = new Intent(this, mainpageParentsActivity.class);
                setResult(Activity.RESULT_OK,intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void goToStudent(View view,int position) {

        Intent intent = new Intent(this, StudentHereActivity.class);
        intent.putExtra("student_id", mystudentsIDsList.get(position));
        intent.putExtra("position",position);
        startActivity(intent);

    }
}
