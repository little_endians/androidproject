package com.example.apple.bambiniapp.dataModelsToCrud;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendStudentRequests {

    @SerializedName("Onay")
    @Expose
    private String onay;

    @SerializedName("Red")
    @Expose
    private String red;

    @SerializedName("user_id")
    @Expose
    private int user_id;

    public SendStudentRequests(String onay, String red,int user_id) {
        this.onay = onay;
        this.red = red;
        this.user_id = user_id;
    }
}
