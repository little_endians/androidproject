package com.example.apple.bambiniapp;

import android.os.AsyncTask;
import android.util.Log;

import com.example.apple.bambiniapp.entities.Announcement;
import com.example.apple.bambiniapp.entities.Comment;
import com.example.apple.bambiniapp.entityServices.AnnouncementService;
import com.example.apple.bambiniapp.entityServices.CommentService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

public class CommentLoader extends AsyncTask<String, Response<ResponseBody>,Response<ResponseBody>> {

    int ntf_id;

    public CommentLoader(int user_id) {
        this.ntf_id = user_id;
    }

    RetrofitConnection rc = new RetrofitConnection();
    Retrofit retrofit = rc.getRetrofit();
    Response<ResponseBody> response;
    ArrayList<Comment> commentList ;

    @Override
    protected Response<ResponseBody> doInBackground(String... strings) {
        CommentService commentService = retrofit.create(CommentService.class);
        commentList = new ArrayList<Comment>();

        Call<ResponseBody> call = commentService.getComments(ntf_id);

        try {

            response = call.execute();

            if(!response.isSuccessful()){
                Log.e("RESPONSE FOR LOGIN", "...RESPONSE FAILED!...");
                return null;
            }

            String jsonData = response.body().string();
            System.out.println("DO-IN-BACK COMMENT-- "+jsonData);
            JSONObject obj = new JSONObject(jsonData);
            JSONArray cmt =  obj.getJSONArray("comment");

            for (int i = 0; i < cmt.length() ; i++) {
                System.out.println(cmt.get(i).toString());
                JSONObject json_ann = new JSONObject(cmt.get(i).toString());
                String date = json_ann.getString("cmmt_creationdate");

                Comment c = new Comment(json_ann.getInt("c_id"),json_ann.getString("cmmt_box"),json_ann.getString("cmmt_tag"),date,json_ann.getString("writer_name"));
                commentList.add(c);
            }

        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return response;
    }
    // Runs after doInBackground.
    @Override
    protected void onPostExecute(Response<ResponseBody> response) {

        if (response == null) {
            Log.e("RESPONSE onPostExecute", "...RESPONSE FAILED!...");
        }

    }

    // Runs in UI before background thread is called
    @Override
    protected void onPreExecute(){

    }


    // This is called from background thread but runs in UI
    @Override
    protected void onProgressUpdate(Response<ResponseBody>... values) {

        // Do things like update the progress bar
    }

}
