package com.example.apple.bambiniapp.dataModelsToCrud;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeleteAnnouncement {

    @SerializedName("ntf_id")
    @Expose
    private int ntf_id;

    public DeleteAnnouncement(int ntf_id) {
        this.ntf_id = ntf_id;
    }
}
