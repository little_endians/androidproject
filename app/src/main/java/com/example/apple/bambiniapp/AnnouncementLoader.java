package com.example.apple.bambiniapp;

import android.os.AsyncTask;
import android.util.Log;

import com.example.apple.bambiniapp.entities.Announcement;
import com.example.apple.bambiniapp.entityServices.AnnouncementService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AnnouncementLoader extends AsyncTask<String, Response<ResponseBody>,Response<ResponseBody>> {

    int user_id;

    public AnnouncementLoader(int user_id) {
        this.user_id = user_id;
    }

    RetrofitConnection rc = new RetrofitConnection();
    Retrofit retrofit = rc.getRetrofit();
    Response<ResponseBody> response;
    ArrayList<Announcement> announcementList ;
    Boolean isPOST = false;

    @Override
    protected Response<ResponseBody> doInBackground(String... strings) {
        AnnouncementService annService = retrofit.create(AnnouncementService.class);
        announcementList = new ArrayList<Announcement>();

        Call<ResponseBody> call = annService.all(user_id);

        try {

            response = call.execute();

            if(!response.isSuccessful()){
                Log.e("RESPONSE FOR LOGIN", "...RESPONSE FAILED!...");
                return null;
            }

            String jsonData = response.body().string();
            System.out.println("DO-IN-BACK ANNOUNCEMENT-- "+jsonData);
            JSONObject obj = new JSONObject(jsonData);
            JSONArray ann =  obj.getJSONArray("announce");

            for (int i = 0; i < ann.length() ; i++) {
                JSONObject json_ann = new JSONObject(ann.get(i).toString());
                System.out.println("DO-IN-BACK ANNOUNCEMENT-- "+ann.get(i));
                String date = json_ann.getString("ntf_creationdate");
                date = date.replaceAll("T"," ");
                date = date.replaceAll("Z","");
                date = date.substring(0,date.indexOf("."));
                Announcement a = new Announcement(json_ann.getInt("ntf_id"),json_ann.getString("subject"),json_ann.getString("text_box"),date,json_ann.getString("writer_name"),json_ann.getString("classname"),json_ann.getString("ntf_tag"),json_ann.getString("evet"),json_ann.getString("hayir"));
                announcementList.add(a);
            }

        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return response;
    }
    // Runs after doInBackground.
    @Override
    protected void onPostExecute(Response<ResponseBody> response) {

        if (response == null) {
            Log.e("RESPONSE onPostExecute", "...RESPONSE FAILED!...");
        }
        this.cancel(true);

    }

    // Runs in UI before background thread is called
    @Override
    protected void onPreExecute(){

    }


    // This is called from background thread but runs in UI
    @Override
    protected void onProgressUpdate(Response<ResponseBody>... values) {

        // Do things like update the progress bar
    }

}
