package com.example.apple.bambiniapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.apple.bambiniapp.dataModelsToCrud.EditAnnouncementDatas;
import com.example.apple.bambiniapp.entities.Announcement;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.example.apple.bambiniapp.mainpageTeachersActivity.announcementsList;

public class editAnnouncementActivity extends AppCompatActivity {

    Button editButton;
    EditText titleText,contentText;
    int ntf_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_announcement);

        String date = getIntent().getExtras().getString("date");
        String title = getIntent().getExtras().getString("title");
        String content = getIntent().getExtras().getString("content");
        ntf_id = getIntent().getExtras().getInt("ntf_id");

        titleText = (EditText) findViewById(R.id.editTitleTextview);
        contentText = (EditText) findViewById(R.id.editContentTextview);
        editButton = (Button) findViewById(R.id.editButton);

        titleText.setText(title);
        contentText.setText(content);

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String editedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime());
                EditedAnnouncementPost ePost = new EditedAnnouncementPost();
                ePost.postingEditedAnn(ntf_id,String.valueOf(titleText.getText()),String.valueOf(contentText.getText()),editedDate);
                goBackAnnouncement(view);
            }
        });
    }

    public void goBackAnnouncement(View view){

        Intent intent = new Intent(this, announcementActivity.class);
        intent.putExtra("editedTitle",String.valueOf(titleText.getText()));
        intent.putExtra("editedContent",String.valueOf(contentText.getText()));
        intent.putExtra("editedDate",new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime()));
        intent.putExtra("edited","true");
        setResult(Activity.RESULT_OK,intent);
        finish();
    }
}
