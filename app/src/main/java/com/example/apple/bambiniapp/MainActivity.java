package com.example.apple.bambiniapp;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.apple.bambiniapp.entities.User;

public class MainActivity extends AppCompatActivity {

    Button loginButton;
    EditText textUsername,textPassword;
    static User u = new User();
    static AuthenticationCheck autCheck;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getSupportActionBar().setTitle("");

        loginButton = (Button) findViewById(R.id.girisButton);
        textUsername = (EditText) findViewById(R.id.emailText);
        textPassword = (EditText) findViewById(R.id.passwordText);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                autCheck = new AuthenticationCheck(); // must be initialized here to do cont. tasks
                String email = textUsername.getText().toString();
                String password = textPassword.getText().toString();
                String[] s = new String[2];
                s[0] = email;
                s[1] = password;
                autCheck.s = s;
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                autCheck.execute(s);

                try {
                    Thread.sleep(1800);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if(autCheck.getCheck() == true)
                {
                    u = autCheck.u;


                    System.out.println("USER NAME "+u.getName());
                    Toast.makeText(getApplicationContext(),
                            "Yönlendiriliyorsunuz...",Toast.LENGTH_SHORT).show();

                    if(u.getTeacher().booleanValue()==true)
                        mainpageTeacher(view);
                    if(u.getTeacher().booleanValue()==false)
                        mainpageParent(view);
                }else{
                    Toast.makeText(getApplicationContext(), "Kullanıcı adı veya şifre hatalı",Toast.LENGTH_SHORT).show();
                }


            }
        });
    }

    public void signUpTeacher(View view) {

        Intent intent = new Intent(this, signUpTeacherActivity.class);
        startActivity(intent);

    }
    public void signUpParent(View view) {

        Intent intent = new Intent(this, signUpParentActivity.class);
        startActivity(intent);

    }
    public void mainpageTeacher(View view)  {

        int user_id = u.getUser_id();

        Intent intent = new Intent(this,mainpageTeachersActivity.class);
        intent.putExtra("user",u);
        startActivity(intent);

    }

    public void mainpageParent(View view)  {

        Intent intent = new Intent(this,mainpageParentsActivity.class);
        intent.putExtra("user",u);
        startActivity(intent);

    }

}