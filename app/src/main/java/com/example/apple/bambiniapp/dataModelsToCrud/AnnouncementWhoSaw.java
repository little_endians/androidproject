package com.example.apple.bambiniapp.dataModelsToCrud;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AnnouncementWhoSaw {

    @SerializedName("id")
    @Expose
    private int id;

    public AnnouncementWhoSaw(int id) {
        this.id = id;
    }
}