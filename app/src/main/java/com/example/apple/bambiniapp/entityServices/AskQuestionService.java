package com.example.apple.bambiniapp.entityServices;

import com.example.apple.bambiniapp.dataModelsToCrud.SendQuestion;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AskQuestionService {

    @POST("msoruSor")
    Call<ResponseBody> create(@Body SendQuestion data);
}
