package com.example.apple.bambiniapp.dataModelsToCrud;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class answerPollAnnouncement {

    @SerializedName("user_id")
    @Expose
    private int user_id;

    @SerializedName("answer")
    @Expose
    private String answer;

    @SerializedName("pass")
    @Expose
    private String pass;

    public answerPollAnnouncement(int user_id, String answer, String pass) {
        this.user_id = user_id;
        this.answer = answer;
        this.pass = pass;
    }
}
