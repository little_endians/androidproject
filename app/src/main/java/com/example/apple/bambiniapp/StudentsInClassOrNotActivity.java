package com.example.apple.bambiniapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.apple.bambiniapp.entities.Student;

import java.util.ArrayList;

public class StudentsInClassOrNotActivity extends AppCompatActivity {

    public static final int STUDENTSACCEPT = 334;

    static HereorNotLoader HNL ;
    static ArrayList<Student> students = new ArrayList<>();
    ArrayList<Student> studentsNotHere = new ArrayList<>();
    TextView swipename;
    String classname,classhour;
    int numOfStudents = 0;
    int counterForSwipe = 0;
    boolean notzero,finish = false;
    Button b;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students_in_class_or_not);

        swipename = (TextView) findViewById(R.id.textView7);
        b = (Button) findViewById(R.id.accept);

        swipename.setText("Baslamak icin sağa kaydır.");
        classname = getIntent().getStringExtra("classname");
        classhour = getIntent().getStringExtra("classhour");

        HNL = new HereorNotLoader(classname);
        HNL.execute();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        students = HNL.studentList;
        numOfStudents = students.size();

        swipename.setText(students.get(0).getName() + " " + students.get(0).getSurname());


        swipename.setOnTouchListener(new OnSwipeTouchListener(StudentsInClassOrNotActivity.this) {
            public void onSwipeTop() {
            }
            public void onSwipeRight() {

                if(counterForSwipe < numOfStudents) {

                    swipename.setText(students.get(counterForSwipe).getName() + " " + students.get(counterForSwipe).getSurname());
                    counterForSwipe = counterForSwipe +1;
                    notzero = true;
                }
                /*
                if(counterForSwipe == numOfStudents)
                    swipename.setText("Onaylayabilirsiniz.");
                */
            }
            public void onSwipeLeft() {

                if(counterForSwipe < numOfStudents) {

                    swipename.setText(students.get(counterForSwipe).getName() + " " + students.get(counterForSwipe).getSurname());
                    studentsNotHere.add(students.get(counterForSwipe));
                    counterForSwipe = counterForSwipe +1;
                    //studentsNotHereText = studentsNotHereText + students.get(counterForSwipe).getName() + " " + students.get(counterForSwipe).getSurname() + "\n";
                    notzero = true;
                }
                /*
                if(counterForSwipe == numOfStudents)
                    swipename.setText("Onaylayabilirsiniz.");
                */

            }
            public void onSwipeBottom() {
            }

        });

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                gotoAccept();
            }});

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.mystudents_menuitems, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.finishstudents:
                turnBackInit();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void turnBackInit(){
        Intent intent = new Intent(this, InitActivityToStudentHereorNot.class);
        setResult(Activity.RESULT_OK,intent);
        finish();

    }
    public void gotoAccept(){
        Intent intent = new Intent(this, ListingStudentsNotHereActivity.class);
        intent.putParcelableArrayListExtra("studentsnothere",studentsNotHere);
        intent.putExtra("classhour",classhour);
        startActivityForResult(intent,STUDENTSACCEPT);
    }
}
