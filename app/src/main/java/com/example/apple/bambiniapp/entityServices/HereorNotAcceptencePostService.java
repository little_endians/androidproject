package com.example.apple.bambiniapp.entityServices;

import com.example.apple.bambiniapp.entities.Student;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface HereorNotAcceptencePostService {

    @GET("myoklamaOnay")
    Call<ResponseBody> postAcceptanceStudents();
}
