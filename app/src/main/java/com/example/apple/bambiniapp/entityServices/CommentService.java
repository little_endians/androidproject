package com.example.apple.bambiniapp.entityServices;

import com.example.apple.bambiniapp.dataModelsToCrud.EditAnnouncementDatas;
import com.example.apple.bambiniapp.dataModelsToCrud.PostCommentDatas;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface CommentService {
    //@GET("msorular/{id}")
    @GET("mduyurular/{id}")
    Call<ResponseBody> getComments(@Path("id") int ntf_id);

    @POST("myorum/{id}")
    Call<ResponseBody> postComment(@Path("id") int id, @Body PostCommentDatas data);
}
