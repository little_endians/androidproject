package com.example.apple.bambiniapp.entityServices;

import com.example.apple.bambiniapp.dataModelsToCrud.EditAnnouncementDatas;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface AnnouncementEditService {

    @POST("mduzenle/{id}")
    Call<ResponseBody> create(@Path("id") int id, @Body EditAnnouncementDatas data);
}
