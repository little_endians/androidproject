package com.example.apple.bambiniapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.example.apple.bambiniapp.adapters.adapterForQuestionPage;
import com.example.apple.bambiniapp.entities.Comment;
import com.example.apple.bambiniapp.entities.Question;

import java.util.ArrayList;

public class AQuestionParentActivity extends AppCompatActivity {

    ArrayList<Question> questionP = new ArrayList<Question>();
    ArrayList<Comment> commentp = new ArrayList<Comment>();
    Comment commentteacher;
    Question q = new Question();
    ListView oneQuestionListviewTeacher;
    adapterForQuestionPage adapterX;
    OneCommentforQuestionLoader cdf;
    int user_id,qid;
    int oneclickforcomment = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aquestion_teacher);

        q = getIntent().getParcelableExtra("question");
        questionP.add(q);

        Question answerTAG = new Question(0,0, "",0, "Cevabı görmek için tıklayın.", "","","");
        questionP.add(answerTAG);

        oneQuestionListviewTeacher = (ListView) findViewById(R.id.oneQuestionListviewTeacher);
        adapterX = new adapterForQuestionPage(this,R.layout.layout_textviewfor1announcement,questionP);
        oneQuestionListviewTeacher.setAdapter(adapterX);

        oneQuestionListviewTeacher.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View v, int position, long id){

                if(position == 1 && oneclickforcomment == 0){

                    user_id = getIntent().getIntExtra("user_id",0);
                    qid = getIntent().getIntExtra("ntf_id",0);

                    cdf = new OneCommentforQuestionLoader(qid);
                    cdf.execute();

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    commentteacher = cdf.cmm;

                    Question q = new Question(0,0,"",0,commentteacher.getComment(),commentteacher.getComment_creationdate(),"","");
                    questionP.add(q);

                    adapterX.notifyDataSetChanged();
                    oneclickforcomment++;

                }

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.mystudents_menuitems, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.finishstudents:
                turnBack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void turnBack(){
        Intent intent = new Intent(this, questionActivitiy.class);
        setResult(Activity.RESULT_OK,intent);
        finish();

    }

    public void goToAnnouncement(View view,int position) {

        Intent intent = new Intent(this, AQuestionTeacherActivity.class);
        intent.putExtra("position",position);
        intent.putExtra("question",questionP.get(position));
        intent.putExtra("user_id",user_id);
        intent.putExtra("ntf_id",questionP.get(position).getQ_id());
        startActivityForResult(intent,1023);
    }


}
