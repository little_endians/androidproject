package com.example.apple.bambiniapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.apple.bambiniapp.entities.Announcement;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.example.apple.bambiniapp.mainpageTeachersActivity.REQUEST_CODE_ADD;

public class addYesnoActivity extends AppCompatActivity {

    EditText yesNotitleText,yesNopriorityText,yesNoannText;
    Button yesNosendAnnButton;

    String title,priority,content,creationdate;
    Announcement announcement;
    int newAnnouncementid;

    int user_id;
    String classname,username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_yesno);

        yesNotitleText = (EditText) findViewById(R.id.yesNotitleText);
        yesNopriorityText = (EditText) findViewById(R.id.yesNopriorityText);
        yesNoannText = (EditText) findViewById(R.id.yesNoannText);
        yesNosendAnnButton = (Button) findViewById(R.id.yesNosendAnnButton);
        newAnnouncementid = getIntent().getIntExtra("newAnnouncementid",0);

        announcement = new Announcement(newAnnouncementid,title,content,creationdate);

        yesNosendAnnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                title = yesNotitleText.getText().toString();
                priority = yesNopriorityText.getText().toString();
                content = yesNoannText.getText().toString();

                creationdate = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime());
                user_id = getIntent().getIntExtra("user_id",23);
                classname = getIntent().getStringExtra("classname");
                username = getIntent().getStringExtra("username");
                NewAnnouncementPost annPost = new NewAnnouncementPost();
                //classname,
                annPost.postingAnn(user_id,username,title,content,creationdate,"yanit");
                Toast.makeText(getApplicationContext(), "Duyuru eklendi ...",Toast.LENGTH_SHORT).show();
                sendAnnGoBack(view);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        getMenuInflater().inflate(R.menu.addannouncement_menuitems, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void sendAnnGoBack(View view) {

        Intent intent = new Intent(this, mainpageTeachersActivity.class);
        Announcement announcement = new Announcement(newAnnouncementid,title,content,creationdate);
        intent.putExtra("addedAnnouncement",announcement);
        setResult(Activity.RESULT_OK,intent);
        finish();

    }

}