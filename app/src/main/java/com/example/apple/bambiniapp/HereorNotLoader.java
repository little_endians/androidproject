package com.example.apple.bambiniapp;

import android.os.AsyncTask;
import android.util.Log;

import com.example.apple.bambiniapp.entities.Announcement;
import com.example.apple.bambiniapp.entities.Student;
import com.example.apple.bambiniapp.entityServices.AnnouncementService;
import com.example.apple.bambiniapp.entityServices.AnnouncementWhoSawService;
import com.example.apple.bambiniapp.entityServices.MyStudentsService;
import com.example.apple.bambiniapp.entityServices.StudentRequestsService;
import com.example.apple.bambiniapp.entityServices.StudentsHerorNotService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

public class HereorNotLoader extends AsyncTask<String, Response<ResponseBody>,Response<ResponseBody>> {

    String classname ;

    public HereorNotLoader(String classname) {
        this.classname = classname;
    }

    RetrofitConnection rc = new RetrofitConnection();
    Retrofit retrofit = rc.getRetrofit();

    Response<ResponseBody> response;
    ArrayList<Student> studentList ;
    Boolean isPOST = false;

    @Override
    protected Response<ResponseBody> doInBackground(String... strings) {
        StudentsHerorNotService studentsService = retrofit.create(StudentsHerorNotService.class);
        studentList = new ArrayList<Student>();

        Call<ResponseBody> call = studentsService.getClassStudents(classname);

        try {

            response = call.execute();

            if(!response.isSuccessful()){
                Log.e("RESPONSE STU. REQUESTS", "RESPONSE FAILED !");
                return null;
            }

            String jsonData = response.body().string();
            System.out.println("DO-IN-BACK STU. REQUESTS "+jsonData);
            JSONObject obj = new JSONObject(jsonData);
            JSONArray whosawlist =  obj.getJSONArray("ogrenci");

            for (int i = 0; i < whosawlist.length() ; i++) {
                JSONObject json_students = new JSONObject(whosawlist.get(i).toString());
                Student student = new Student(json_students.getInt("s_id"),json_students.getString("name"),json_students.getString("surname"),json_students.getString("courselist"),json_students.getString("class_section"),json_students.getString("onay"),json_students.getString("p_id"));
                studentList.add(student);
            }

        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return response;
    }
    // Runs after doInBackground.
    @Override
    protected void onPostExecute(Response<ResponseBody> response) {

        if (response == null) {
            Log.e("RESPONSE onPostExecute", "...RESPONSE FAILED!...");
        }

    }

    // Runs in UI before background thread is called
    @Override
    protected void onPreExecute(){

    }


    // This is called from background thread but runs in UI
    @Override
    protected void onProgressUpdate(Response<ResponseBody>... values) {

        // Do things like update the progress bar
    }

}
