package com.example.apple.bambiniapp;

import android.os.AsyncTask;
import android.util.Log;

import com.example.apple.bambiniapp.entities.Question;
import com.example.apple.bambiniapp.entityServices.ListQuestionService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

public class QuestionLoader extends AsyncTask<String, Response<ResponseBody>,Response<ResponseBody>> {

    OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(100, TimeUnit.SECONDS)
            .readTimeout(100,TimeUnit.SECONDS).build();
    int user_id;
    RetrofitConnection rc = new RetrofitConnection();
    Retrofit retrofit = rc.getRetrofit();
    private Response<ResponseBody> response;
    public ArrayList<Question> questionList ;
    ListQuestionService listService;

    public QuestionLoader(int user_id) {
        this.user_id = user_id;
        Log.e("USER_ID FOR Question", "...İD:::::!..." + this.user_id);
    }
    public QuestionLoader(){       }

    @Override
    protected Response<ResponseBody> doInBackground(String... strings) {

        Call<ResponseBody> call = listService.all(user_id);

        try {

            response = call.execute();

            if(!response.isSuccessful()){
                Log.e("RESPONSE FOR Question", "...RESPONSE FAILED!...");
                return null;
            }

            String jsonData = response.body().string();
            System.out.println("DO-IN-BACK QUESTION-- "+jsonData);
            JSONObject obj = new JSONObject(jsonData);
            JSONArray ques =  obj.getJSONArray("announce");

            for (int i = 0; i < ques.length() ; i++) {
                JSONObject json_ann = new JSONObject(ques.get(i).toString());
                System.out.println("DO-IN-BACK Quest-- "+ques.get(i));
                String date = json_ann.getString("ntf_creationdate");
                date = date.replaceAll("T"," ");
                date = date.replaceAll("Z","");
                date = date.substring(0,date.indexOf("."));

                Question q = new Question(json_ann.getInt("ntf_id"), json_ann.getInt("writer_id"),json_ann.getString("writer_name"),json_ann.getInt("teacher"),json_ann.getString("text_box"),date,json_ann.getString("subject"),json_ann.getString("ntf_tag"));
                questionList.add(q);



            }

        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return response;
    }

    // Runs after doInBackground.
    @Override
    protected void onPostExecute(Response<ResponseBody> response) {

        if (response == null) {
            Log.e("RESPONSE onPostExecute", "...RESPONSE FAILED!...");
        }

    }

    // Runs in UI before background thread is called
    @Override
    protected void onPreExecute(){
        listService = retrofit.create(ListQuestionService.class);
        questionList = new ArrayList<Question>();
    }


    // This is called from background thread but runs in UI
    @Override
    protected void onProgressUpdate(Response<ResponseBody>... values) {

        // Do things like update the progress bar
    }

    public ArrayList<Question> getQuestionList() {
        return questionList;
    }
}
