package com.example.apple.bambiniapp.entityServices;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ListQuestionService {

    @GET("msorulariGor/{user_id}")
    Call<ResponseBody> all(@Path("user_id") int user_id);

}
