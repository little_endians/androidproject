package com.example.apple.bambiniapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.apple.bambiniapp.adapters.adapterStudentRequests;

import java.util.ArrayList;

public class StudentRequestsActivity extends AppCompatActivity {

    static StudentRequestsLoader SL ;
    static ArrayList<String> studentsList = new ArrayList<>();
    static ArrayList<Integer> studentsIDsList = new ArrayList<>();
    adapterStudentRequests adapterS;
    ListView ourList;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        MenuItem classNameItem;
        getMenuInflater().inflate(R.menu.studentrequests_menuitems, menu);


        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_requests);

        SL = new StudentRequestsLoader(0);
        SL.execute();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        studentsList = SL.studentRequestsList;
        studentsIDsList = SL.studentIDs;
        //Log.e("studen list 0.",studentsList.get(0));
        ourList = (ListView) findViewById(R.id.requestsListview);
        adapterS = new adapterStudentRequests(this,R.layout.layout_textviewforlistr,studentsList);
        ourList.setAdapter(adapterS);

        ourList.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(final AdapterView<?> parent, View view, final int position, long id) {

                AlertDialog.Builder builder = new AlertDialog.Builder(StudentRequestsActivity.this);
                builder.setTitle("Bu öğrenciyi eklemek istiyor musunuz ?");
                builder.setPositiveButton("EVET", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        StudentCrudOps reqPost = new StudentCrudOps();
                        reqPost.postingRequest("Onayla"," ",studentsIDsList.get(position));
                        studentsList.remove(position);
                        adapterS.notifyDataSetChanged();
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("HAYIR", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        StudentCrudOps reqPost = new StudentCrudOps();
                        reqPost.postingRequest(" ","Reddet",studentsIDsList.get(position));
                        studentsList.remove(position);
                        adapterS.notifyDataSetChanged();
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();

            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.finish:
                Intent intent = new Intent(this, mainpageTeachersActivity.class);
                setResult(Activity.RESULT_OK,intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
