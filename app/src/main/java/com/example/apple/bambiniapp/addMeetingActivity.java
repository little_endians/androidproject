package com.example.apple.bambiniapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.apple.bambiniapp.entities.Announcement;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class addMeetingActivity extends AppCompatActivity {

    EditText meetingdateText,meetingPlaceText,startHourText, finishHourText;
    String title,content,creationdate,meetingdate,startHour,finishHour,meetingPlace;
    int newAnnouncementid;
    Button sendAnnButton;

    int user_id;
    String class_name;
    String username;

    Announcement announcement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_meeting);

        meetingdateText = (EditText) findViewById(R.id.meeting_date);
        meetingPlaceText = (EditText) findViewById(R.id.meeting_place);
        startHourText = (EditText) findViewById(R.id.meeting_starthour);
        finishHourText = (EditText) findViewById(R.id.meeting_finishhour);
        sendAnnButton = (Button) findViewById(R.id.add_meeting_ann);
        newAnnouncementid = getIntent().getIntExtra("newAnnouncementid",0);

        announcement = new Announcement(newAnnouncementid,title,content,creationdate);

        sendAnnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                title = "Veli Toplantısı";
                meetingdate = meetingPlaceText.getText().toString();
                startHour = startHourText.getText().toString();
                finishHour = finishHourText.getText().toString();
                meetingPlace = meetingPlaceText.getText().toString();
                content = "Öğrencinizin sınıf öğretmeniyim. "+ meetingdate +" tarihinde, " + startHour + " - " + finishHour + " saatleri arasında "
                + meetingPlace + "'de/ 'da " +" toplantı düzenlemek istiyorum. Katılım durumunu lütfen en kısa süre içinde bildiriniz. Gereğini saygılarımla arz ederim." ;


                creationdate = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime());
                user_id = getIntent().getIntExtra("user_id",23);
                class_name = getIntent().getStringExtra("classname");
                username = getIntent().getStringExtra("username");
                NewAnnouncementPost annPost = new NewAnnouncementPost();
                annPost.postingSpeAnn(meetingdate,startHour,finishHour,meetingPlace,user_id,username,"Veli Toplantısı",creationdate);
                Toast.makeText(getApplicationContext(), "Duyuru eklendi ...",Toast.LENGTH_SHORT).show();
                sendAnnGoBack(view);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void sendAnnGoBack(View view) {

        Intent intent = new Intent(this, mainpageTeachersActivity.class);
        Announcement announcement = new Announcement(newAnnouncementid,title,content,creationdate);
        intent.putExtra("addedAnnouncement",announcement);
        setResult(Activity.RESULT_OK,intent);
        finish();

    }
}
