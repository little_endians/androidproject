package com.example.apple.bambiniapp;

import android.os.AsyncTask;
import android.util.Log;

import com.example.apple.bambiniapp.entityServices.AnnouncementWhoSawService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

public class WhoSawAnnLoader extends AsyncTask<String, Response<ResponseBody>,Response<ResponseBody>> {

    int ann_id = 91;

    public WhoSawAnnLoader(int ann_id) {
        this.ann_id = ann_id;
    }

    RetrofitConnection rc = new RetrofitConnection();
    Retrofit retrofit = rc.getRetrofit();
    Response<ResponseBody> response;
    ArrayList<String> whosawList ;
    Boolean isPOST = false;

    @Override
    protected Response<ResponseBody> doInBackground(String... strings) {
        AnnouncementWhoSawService whosawService = retrofit.create(AnnouncementWhoSawService.class);
        whosawList = new ArrayList<String>();

        Call<ResponseBody> call = whosawService.getList(ann_id);

        try {

            response = call.execute();

            if(!response.isSuccessful()){
                Log.e("RESPONSE FOR WHO SAW", "RESPONSE FAILED !");
                return null;
            }

            String jsonData = response.body().string();
            System.out.println("DO-IN-BACK WHO SAW "+jsonData);
            JSONObject obj = new JSONObject(jsonData);
            JSONArray whosawlist =  obj.getJSONArray("list");

            for (int i = 0; i < whosawlist.length() ; i++) {
                JSONObject json_whosaw = new JSONObject(whosawlist.get(i).toString());
                String name = json_whosaw.getString("name") + " " + json_whosaw.getString("surname");
                whosawList.add(name);
            }

        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return response;
    }
    // Runs after doInBackground.
    @Override
    protected void onPostExecute(Response<ResponseBody> response) {

        if (response == null) {
            Log.e("RESPONSE onPostExecute", "...RESPONSE FAILED!...");
        }

    }

    // Runs in UI before background thread is called
    @Override
    protected void onPreExecute(){

    }


    // This is called from background thread but runs in UI
    @Override
    protected void onProgressUpdate(Response<ResponseBody>... values) {

        // Do things like update the progress bar
    }

}
