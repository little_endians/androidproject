package com.example.apple.bambiniapp.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Comment implements Parcelable {

    @SerializedName("c_id")
    @Expose
    private int comment_id;

    @SerializedName("cmmt_box")
    @Expose
    private String comment;

    @SerializedName("cmmt_tag")
    @Expose
    private String comment_tag;

    @SerializedName("cmmt_creationdate")
    @Expose
    private String comment_creationdate;

    @SerializedName("writer_name")
    @Expose
    private String writer_name;

    public Comment() {

    }

    public Comment(int comment_id, String comment, String comment_tag, String comment_creationdate, String writer_name) {
        this.comment_id = comment_id;
        this.comment = comment;
        this.comment_tag = comment_tag;
        this.comment_creationdate = comment_creationdate;
        this.writer_name = writer_name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment_tag() {
        return comment_tag;
    }

    public void setComment_tag(String comment_tag) {
        this.comment_tag = comment_tag;
    }

    public String getComment_creationdate() {
        return comment_creationdate;
    }

    public void setComment_creationdate(String comment_creationdate) {
        this.comment_creationdate = comment_creationdate;
    }

    public int getComment_id() {
        return comment_id;
    }

    public void setComment_id(int comment_id) {
        this.comment_id = comment_id;
    }

    public String getWriter_name() {
        return writer_name;
    }

    public void setWriter_name(String writer_name) {
        this.writer_name = writer_name;
    }

    public Comment(Parcel in){
        String[] data= new String[5];
        in.readStringArray(data);

        this.comment_id = Integer.parseInt(data[0]);
        this.comment = data[1];
        this.comment_tag = data[2];
        this.comment_creationdate = data[3];
        this.writer_name = data[4];

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeStringArray(new String[]{String.valueOf(this.comment_id),this.comment,this.comment_tag,this.comment_creationdate,this.writer_name});
    }

    public static final Parcelable.Creator<Comment> CREATOR= new Parcelable.Creator<Comment>() {

        @Override
        public Comment createFromParcel(Parcel source) {

            return new Comment(source);  //using parcelable constructor
        }

        @Override
        public Comment[] newArray(int size) {

            return new Comment[size];
        }
    };

}
