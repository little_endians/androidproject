package com.example.apple.bambiniapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.apple.bambiniapp.entities.Announcement;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class addTravelActivity extends AppCompatActivity {

    EditText traveldateText,travelPlaceText,startHourText, finishHourText;
    String title,content,creationdate,traveldate,startHour,finishHour,travelPlace;
    int newAnnouncementid,user_id;
    Button sendAnnButton;

    Announcement announcement;
    String classname,username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_travel);

        traveldateText = (EditText) findViewById(R.id.travel_date);
        travelPlaceText = (EditText) findViewById(R.id.travel_place);
        startHourText = (EditText) findViewById(R.id.travel_starthour);
        finishHourText = (EditText) findViewById(R.id.travel_finishhour);
        sendAnnButton = (Button) findViewById(R.id.add_travel_ann);
        newAnnouncementid = getIntent().getIntExtra("newAnnouncementid",0);

        announcement = new Announcement(newAnnouncementid,title,content,creationdate);

        sendAnnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                title = "Gezi";
                traveldate = traveldateText.getText().toString();
                startHour = startHourText.getText().toString();
                finishHour = finishHourText.getText().toString();
                travelPlace = travelPlaceText.getText().toString();
                content = "Öğrencinizin sınıf öğretmeniyim. "+ traveldate +" tarihinde, " + startHour + " - " + finishHour + " saatleri arasında "
                        + travelPlace+ "'ne/'na " +" gezi düzenlemek istiyorum. Çocuğunuzun katılım durumunu lütfen en kısa süre içinde bildiriniz. Gereğini saygılarımla arz ederim." ;


                creationdate = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime());
                user_id = getIntent().getIntExtra("user_id",23);
                classname = getIntent().getStringExtra("classname");
                username = getIntent().getStringExtra("username");
                NewAnnouncementPost annPost = new NewAnnouncementPost();
                annPost.postingSpeAnn(traveldate,startHour,finishHour,travelPlace,user_id,username,"Gezi",creationdate);
                Toast.makeText(getApplicationContext(), "Duyuru eklendi ...",Toast.LENGTH_SHORT).show();
                sendAnnGoBack(view);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void sendAnnGoBack(View view) {

        Intent intent = new Intent(this, mainpageTeachersActivity.class);
        Announcement announcement = new Announcement(newAnnouncementid,title,content,creationdate);
        intent.putExtra("addedAnnouncement",announcement);
        setResult(Activity.RESULT_OK,intent);
        finish();

    }

}
