package com.example.apple.bambiniapp;

import android.util.Log;

import com.example.apple.bambiniapp.dataModelsToCrud.NewAnnouncementDatas;
import com.example.apple.bambiniapp.dataModelsToCrud.SendQuestion;
import com.example.apple.bambiniapp.entities.Student;
import com.example.apple.bambiniapp.entityServices.AnnouncementService;
import com.example.apple.bambiniapp.entityServices.AskQuestionService;
import com.example.apple.bambiniapp.entityServices.SendStudentsNotInClass;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class NewHereOrNotListPost  {

    RetrofitConnection rc = new RetrofitConnection();
    Retrofit retrofit = rc.getRetrofit();

    public void postingHereOrNotList(int classhour,ArrayList<Student> students) {

        SendStudentsNotInClass sendService = retrofit.create(SendStudentsNotInClass.class);
        Call<ResponseBody> call = sendService.postStudents(classhour,students);

        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {

                    try {
                        Log.e("Ann post return", response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("FAIL", "onFailure ");

            }
        });

    }
}